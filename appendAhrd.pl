#!/usr/bin/env perl

use strict;
use warnings;

use Bio::Tools::GFF;
use Bio::SeqFeature::Generic;
use File::Spec;
use Getopt::Long;
use IO::Uncompress::AnyUncompress;
use Pod::Usage;

our $delim = "\t";
my $man = 0;
my $help = 0;
# Custom tags should start with a lower-case letter
my $ahrdTag = "ahrd";
my $ahrdQcTag = "ahrd-qc";
my ($pathAnnot, $pathAhrd, $pathOut);

GetOptions(
		"h|help" => \$help,
		"o=s"    => \$pathOut,
		"annot=s"    => \$pathAnnot,
		"ahrd=s"     => \$pathAhrd,
		"ahrdTag=s"  => \$ahrdTag,
		"ahrdQcTag=s"=> \$ahrdQcTag,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Annotation file is required") unless $pathAnnot;
die("AHRD file is required") unless $pathAhrd;

my $fhOut;
if ($pathOut) {
	open($fhOut, '>', $pathOut) or die("Unable to write to the output file ($pathOut). $!");
} else {
	$fhOut = \*STDOUT;
}


my $fhAhrd = new IO::Uncompress::AnyUncompress($pathAhrd);
my %ahrd;
while (<$fhAhrd>) {
	chomp;
	next if /^#/;
	my ($geneId, $blastAcc, $ahrdQc, $ahrd, $interpro, $goTerm) = split($delim);
	# If geneId=0, we lose it but why would you set geneId to 0?
	next unless $geneId;
	$ahrd{$geneId} = { -ahrd => $ahrd, -qc => $ahrdQc };
}
close($fhAhrd);


my $fhAnn = new IO::Uncompress::AnyUncompress($pathAnnot);
my $gffAnn = Bio::Tools::GFF->new(-fh => $fhAnn, -gff_version => 3);
# This may or may not speed up the processing
$gffAnn->ignore_sequence(1);
my $gffOut = Bio::Tools::GFF->new(-fh => $fhOut, -gff_version => 3);
while (my $f = $gffAnn->next_feature()) {
	if ($f->primary_tag =~ /^gene$/i) {
		my ($fId) = $f->get_tag_values('ID');
		# If ID is not defined, fall back to Name
		($fId) = $f->get_tag_values('Name') unless $fId;
		# If it is still undefined, the file is malformed
		die("GFF annotation file has invalid format. " .
				"All gene records must have either ID or Name tag.") unless $fId;
		my $ahrdHash = $ahrd{$fId};
		if (defined $ahrdHash) {
			my $ahrdStr = $ahrdHash->{'-ahrd'};
			my $ahrdQc  = $ahrdHash->{'-qc'};
			$f->add_tag_value($ahrdTag, $ahrdStr) if $ahrdStr;
			$f->add_tag_value($ahrdQcTag, $ahrdQc) if $ahrdQc;
		}
	}
	$gffOut->write_feature($f);
}


__END__

=head1 NAME

appendAhrd.pl - appends AHRD tags to the annotation

=head1 SYNOPSIS

appendAhrd.pl --annot gene_annot.gff --ahrd ahrd.csv -o output.gff

=head1 OPTIONS

=over 8

=item B<--annot>

Path to the annotation file in GFF format.

=item B<--ahrd>

Path to the AHRD output file.

=item B<-o>

(Optional) Output GFF file. If not specified, the output is written to STDOUT.

=item B<--ahrdTag>

(Optional) Tag for the AHRD attribute (Human-Readable-Description).

=item B<--ahrdQcTag>

(Optional) Tag for the AHRD quality code attribute.

=back

=head1 DESCRIPTION

Appends AHRD himan readable description and AHRD quality code to the genes in the input GFF file.
Only gene records are modified. If a gene does not exist in the AHRD file or the human readable
description is empty, the ahrd attribute is not added. If AHRD quality code is not provided (e.g. for
unknown proteins), the ahrd-qc attribute is not added.

Note 1: Custom attributes should start with a lower case letter.

Note 2: Since ",=;" characters are URL-escaped, you may see numerous '%2C' instances in the output.
Comma separates multiple values within the same tag.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut



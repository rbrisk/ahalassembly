use strict;
use warnings;

use Test::Exception;
use Test::More tests => 6;

BEGIN {
	use_ok("AnnotComp");
}

require_ok("AnnotComp");

# Arguments: $genes - an array of gene hashes. Gene has two keys s (strand) and t (transcript
# array).  Transcript array has [utr5, utr3] as the first element. The rest are CDS coordinate
# arrays consisting of beg and end positions. For example, the following record has two genes. The
# first gene is on the + strand and has two transcript. The first transcript has no UTR regions and
# two exons (CDS). The second transcript has both UTR regions and three exons (CDS). The order of
# UTRs is the same regardless of the strand. Note that for genes on '-' strand utr5 (first value) is
# added to the last exon end position.
# my $s = geneArrToString([
#     { s => '+', t => [
#           [[0,0], [30,40], [60,75]],
#           [[2,3], [10,20], [30,40], [60,75]] 
#        ] },
#     { s => '-', t => [ [[3,1], [80,95], [110,130]] ] }
#  ]);
#
# $lastGeneId - counter for gene ids indicating the last used gene id
# $useTranscript - flag specifying whether 'transcript' type should be used to denote transcripts. By
# default, transcripts have 'mRNA' type.
#
# *** This method cannot create models with exons that are entirely untranslated. ***
sub geneArrToStr {
	my ($genes, $lastGidRef, $useTranscript) = @_;

	my $geneCnt;
	my @src = qw/ s1 src /;
	my $gffStr = "";
	my $tLbl = $useTranscript ? 'transcript' : 'mRNA';
	for my $i (0..$#$genes) {
		++$$lastGidRef;
		my $g = $genes->[$i];
		my $strand = $g->{'s'};
		my @qual = ('.', $strand, '.');
		my $gId = "g" . $$lastGidRef;
		# Beg of the first exon of the first transcript
		my ($gBeg, $gEnd) = ($g->{'t'}[0][1][0], 0);
		my $childStr = "";

		for my $j (0..$#{$g->{'t'}}) {
			my $t = $g->{'t'}[$j];
			my $tId = join('.', $gId, "t$j");
			my $tAttr = "ID=$tId;Parent=$gId";
			my $utr5 = $t->[0][0];
			my $utr3 = $t->[0][1];
			my $beg = $t->[1][0];
			my $end = $t->[-1][1];
			$beg -= $strand eq '+' ? $utr5 : $utr3;
			$end += $strand eq '+' ? $utr3 : $utr5;
			$gBeg = $beg if $beg < $gBeg;
			$gEnd = $end if $end > $gEnd;
			$childStr .= join("\t", @src, $tLbl, $beg, $end, @qual, $tAttr) . "\n";
			for my $k (1..$#$t) {
				my $eId = join('.', $tId, "e$k");
				my $eAttr = "ID=$eId;Parent=$tId";
				my $cId = join('.', $tId, "c$k");
				my $cAttr = "ID=$cId;Parent=$tId";
				my ($beg, $end) = @{$t->[$k]};
				$childStr .= join("\t", @src, 'CDS', $beg, $end, @qual, $cAttr) . "\n";
				if ($k == 1) {
					$beg -= $strand eq '+' ? $utr5 : $utr3;
				} elsif ($k == $#$t) {
					$end += $strand eq '+' ? $utr3 : $utr5;
				}
				$childStr .= join("\t", @src, 'exon', $beg, $end, @qual, $eAttr) . "\n";
			}
		}
		$gffStr .= join("\t", @src, 'gene', $gBeg, $gEnd, @qual, "ID=$gId") . "\n";
		$gffStr .= $childStr;
	}
	return $gffStr;
}


sub getPre {
	my ($lastGidRef) = @_;
	my $genes = [
		{ s => '+', t => [
				[[0,0], [30,40], [60, 75]],
				[[3,6], [10,21], [30, 40], [60,75]] ] },
		{ s => '-', t => [
				[[0,9], [101, 120], [151, 190]] ] }
	];
	return geneArrToStr($genes, $lastGidRef);
}


sub getPost {
	my ($lastGidRef) = @_;
	my $genes = [
		{ s => '+', t => [
				[[0,0], [10021,10040], [10071,10120], [10256,10295]] ] },
		{ s => '-', t => [
				[[2,4], [10343, 10442], [10501,10700]] ] }
	];
	return geneArrToStr($genes, $lastGidRef);
}


sub loadData {
	my %args = @_;
	my $obj = $args{'-obj'};
	my $lbl = $args{'-label'};
	my $lastGidRef = $args{'-lastGidRef'};
	my $genes = $args{'-genes'};
	my $woPre = $args{'-woPre'};
	my $woPost= $args{'-woPost'};

	my $gffStr = "";
	$gffStr .= getPre($lastGidRef) unless $woPre;
	$gffStr .= geneArrToStr($genes, $lastGidRef) if $genes;
	$gffStr .= getPost($lastGidRef) unless $woPost;

	open(my $fh, "<", \$gffStr) or die("Stringstream error. $!");
	my $loadCounts = $obj->loadData($fh, $lbl);
	$fh->close();
	return $loadCounts;
}


subtest "DB loading" => sub {
	plan tests => 11;

	my $lastGidA = 0;
	my $lastGidB = 0;
	my $genesA = [
			{ s => '-', t => [ [[0,0], [1301, 1600]] ] } 
		];
	my $obj = AnnotComp->new();
	my $loadCountsA = loadData(
			-obj   => $obj, 
			-label => 'AnnotA', 
			-lastGidRef => \$lastGidA, 
			-genes => $genesA,
		);
	my $loadCountsB = loadData(-obj => $obj, -label => 'AnnotB', -lastGidRef => \$lastGidB);
	is_deeply(
			$loadCountsA,
			{ -geneN => 5, -mRnaN => 6, -cdsN => 13 },
			'Count reporting A',
		);
	is_deeply(
			$loadCountsB,
			{ -geneN => 4, -mRnaN => 5, -cdsN => 12 },
			'Count reporting B'
		);

	my @genesA = $obj->{'db'}->features(-type => 'gene:AnnotA');
	my @genesB = $obj->{'db'}->features(-type => 'gene:AnnotB');
	cmp_ok(scalar(@genesA), '==', 5, "Loaded 5 genes from A");
	cmp_ok(scalar(@genesB), '==', 4, "Loaded 4 genes from B");

	# AnnotComp changes transcript to mRNA to facilitate processing
	my @transcriptsA = $obj->{'db'}->features(-type => 'mRNA:AnnotA');
	my @transcriptsB = $obj->{'db'}->features(-type => 'mRNA:AnnotB');
	cmp_ok(scalar(@transcriptsA), '==', 6, "Loaded 6 mRNA from A");
	cmp_ok(scalar(@transcriptsB), '==', 5, "Loaded 5 mRNA from B");

	my @exonsA = $obj->{'db'}->features(-type => 'exon:AnnotA');
	my @exonsB = $obj->{'db'}->features(-type => 'exon:AnnotB');
	cmp_ok(scalar(@exonsA), '==', 13, "Loaded 13 exons from A");
	cmp_ok(scalar(@exonsB), '==', 12, "Loaded 12 exons from B");

	my @cdsA = $obj->{'db'}->features(-type => 'CDS:AnnotA');
	my @cdsB = $obj->{'db'}->features(-type => 'CDS:AnnotB');
	cmp_ok(scalar(@cdsA), '==', 13, "Loaded 13 CDS from A");
	cmp_ok(scalar(@cdsB), '==', 12, "Loaded 12 CDS from B");

	my $gId = $genesA[0]->primary_id;
	my %cdsGeneIds;
	for my $t ($genesA[0]->get_SeqFeatures('mRNA')) {
		for my $c ($t->get_SeqFeatures('CDS')) {
			my ($cdsGId) = $c->get_tag_values('geneId');
			$cdsGeneIds{$cdsGId} = 1;
		}
	}
	is_deeply(\%cdsGeneIds, { $gId => 1 }, 'Set parental gene ids');
};


sub countGeneMatches {
	my ($genesA, $genesB) = @_;
	my $lblA = 'AnnotA';
	my $lblB = 'AnnotB';
	my $lastGidA = 0;
	my $lastGidB = 0;
	my $obj = AnnotComp->new();
	loadData(-obj => $obj, -label => $lblA, -lastGidRef => \$lastGidA, -genes => $genesA);
	loadData(-obj => $obj, -label => $lblB, -lastGidRef => \$lastGidB, -genes => $genesB);
	return $obj->countGeneMatches($lblA, $lblB);
}


subtest 'Gene level matching' => sub {
	# Cases
	# 1. Sanity check: supplying a pair of matching genes works
	# 2. Different strands
	# 3. Different number of transcripts (also tests partial match count)
	# 4. Different number of CDS
	# 5. Different 5' UTR length
	# 6. Different 3' UTR length
	# 7. Different CDS length
	plan tests => 7;

	# Sanity check: if we supply a pair of matching genes, it will be detected
	my $genesA = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	my $genesB = $genesA;
	is_deeply(
			countGeneMatches($genesA, $genesB), 
			{ -completeN => 5, -mRnaN => 0},
			'Sanity check'
		);


	# Different strands
	$genesA = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	$genesB = [
			{ s => '-', t => [ [[9,30], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	is_deeply(
			countGeneMatches($genesA, $genesB), 
			{ -completeN => 4, -mRnaN => 0 }, 
			'Different strands'
		);

	# Extra transcript
	$genesA = [
			{ s => '+', t => [ 
					[[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]],
					[[30,9], [1301, 1600], [1631, 1930], [2411, 2440]] 
				] }
		];
	$genesB = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	is_deeply(
			countGeneMatches($genesA, $genesB), 
			{ -completeN => 4, -mRnaN => 1 },
			'Extra transcript and partial match'
		);

	# Extra CDS
	$genesA = [
			{ s => '+', t => [ 
					[[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]],
					[[30,9], [1301, 1600], [2411, 2440]] 
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]],
					[[30,9], [1301, 1600], [1631, 1930], [2411, 2440]] 
				] }
		];
	is_deeply(
			countGeneMatches($genesA, $genesB), 
			{ -completeN => 4, -mRnaN => 1 },
			'Extra CDS and partial match'
		);

	# Different 5' UTR length
	$genesA = [
			{ s => '+', t => [ [[27,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	$genesB = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	is_deeply(
			countGeneMatches($genesA, $genesB), 
			{ -completeN => 4, -mRnaN => 0 },
			"Different 5' UTR length"
		);

	# Different 3' UTR length
	$genesA = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	$genesB = [
			{ s => '+', t => [ [[30,6], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	is_deeply(
			countGeneMatches($genesA, $genesB), 
			{ -completeN => 4, -mRnaN => 0 },
			"Different 3' UTR length"
		);

	# Different CDS length
	$genesA = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1634, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	$genesB = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	is_deeply(
			countGeneMatches($genesA, $genesB), 
			{ -completeN => 4, -mRnaN => 0 },
			"Different CDS length"
		);

};



sub countMRnaMatches {
	my ($genesA, $genesB) = @_;
	my $lblA = 'AnnotA';
	my $lblB = 'AnnotB';
	my $lastGidA = 0;
	my $lastGidB = 0;
	my $obj = AnnotComp->new();
	loadData(-obj => $obj, -label => $lblA, -lastGidRef => \$lastGidA, -genes => $genesA);
	loadData(-obj => $obj, -label => $lblB, -lastGidRef => \$lastGidB, -genes => $genesB);
	return $obj->countMRnaMatches($lblA, $lblB);
}



subtest 'Transcript level matching' => sub {
	# Cases
	# 1. Sanity check: supplying a pair of matching transcripts works
	# 2. Different strands
	# 3. Different number of CDS
	# 4. Different 5' UTR length
	# 5. Different 3' UTR length
	# 6. Different length of both 5' and 3' UTR
	# 7. Different CDS length
	# 8. Multiple transcripts within a gene do not affect the matching pair
	plan tests => 8; 

	# Sanity check: if we supply a pair of matching transcripts, it will be detected
	my $genesA = [
			{ s => '+', t => [ 
					[[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]],
					[[30,9], [1301, 1600], [2011, 2310], [2411, 2440]],
				] }
		];
	my $genesB = $genesA;
	is_deeply(
			countMRnaMatches($genesA, $genesB),
			{ -completeN => 7, -diffUtrN => 0, -diffUtr3N => 0, -diffUtr5N => 0 },
			'Sanity check'
		);
	
	# Different strands
	$genesA = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	$genesB = [
			{ s => '-', t => [ [[9,30], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	is_deeply(
			countMRnaMatches($genesA, $genesB),
			{ -completeN => 5, -diffUtrN => 0, -diffUtr3N => 0, -diffUtr5N => 0 },
			'Different strands'
		);

	# Extra CDS
	$genesA = [
			{ s => '+', t => [ 
					[[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]],
					[[30,9], [1301, 1600], [2411, 2440]] 
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]],
					[[30,9], [1301, 1600], [1631, 1930], [2411, 2440]] 
				] }
		];
	is_deeply(
			countMRnaMatches($genesA, $genesB),
			{ -completeN => 6, -diffUtrN => 0, -diffUtr3N => 0, -diffUtr5N => 0 },
			'Extra CDS'
		);

	# Different 5' UTR length
	$genesA = [
			{ s => '+', t => [ [[27,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	$genesB = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	my $tmpGid = 0;
	is_deeply(
			countMRnaMatches($genesA, $genesB),
			{ -completeN => 5, -diffUtrN => 0, -diffUtr3N => 0, -diffUtr5N => 1 },
			"Different 5' UTR length"
		);

	# Different 3' UTR length
	$genesA = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	$genesB = [
			{ s => '+', t => [ [[30,6], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	is_deeply(
			countMRnaMatches($genesA, $genesB),
			{ -completeN => 5, -diffUtrN => 0, -diffUtr3N => 1, -diffUtr5N => 0 },
			"Different 3' UTR length"
		);

	# Different length of both 5' and 3' UTR
	$genesA = [
			{ s => '+', t => [ [[27,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	$genesB = [
			{ s => '+', t => [ [[30,6], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	is_deeply(
			countMRnaMatches($genesA, $genesB),
			{ -completeN => 5, -diffUtrN => 1, -diffUtr3N => 0, -diffUtr5N => 0 },
			"Different length of both 5' and 3' UTR"
		);

	# Different CDS length
	$genesA = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1634, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	$genesB = [
			{ s => '+', t => [ [[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]] ] }
		];
	is_deeply(
			countMRnaMatches($genesA, $genesB),
			{ -completeN => 5, -diffUtrN => 0, -diffUtr3N => 0, -diffUtr5N => 0 },
			"Different CDS length"
		);

	# Multiple transcripts within a gene do not affect the matching pair
	$genesA = [
			{ s => '+', t => [ 
					[[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]],
					[[30,9], [1301, 1600], [2011, 2310], [2411, 2440]] 
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]],
					[[30,9], [1301, 1600], [1631, 1930], [2411, 2440]] 
				] }
		];
	is_deeply(
			countMRnaMatches($genesA, $genesB),
			{ -completeN => 6, -diffUtrN => 0, -diffUtr3N => 0, -diffUtr5N => 0 },
			'Multiple transcripts within a gene do not affect the matching pair'
		);
};



sub findFused {
	my ($genesA, $genesB) = @_;
	my $lblA = 'AnnotA';
	my $lblB = 'AnnotB';
	my $lastGidA = 0;
	my $lastGidB = 0;
	my $obj = AnnotComp->new();
	loadData(-obj => $obj, -label => $lblA, -lastGidRef => \$lastGidA, -genes => $genesA);
	loadData(-obj => $obj, -label => $lblB, -lastGidRef => \$lastGidB, -genes => $genesB);
	return $obj->findFusedGenes($lblA, $lblB);
}


subtest 'Fused gene count' => sub {
	# Cases
	# 1. Sanity check: do not report matching genes
	# 2. Simple case: two models cleanly fused
	# 3. Complete fusion with varying CDS boundaries
	# 4. Complete fusion with varying UTR boundaries
	# 5. Complete fusion of three genes
	# 6. Partial fusion
	# 7. Partial fusion with varying CDS boundaries
	# 8. Partial fusion of three genes
	# 7. Skip genes with overlapping UTRs but without overlapping CDS
	plan tests => 9;

	# Sanity check: do not report matching genes
	my $genesA = [
			{ s => '+', t => [ 
					[[30,9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440]],
					[[30,9], [1301, 1600], [2011, 2310], [2411, 2440]],
				] }
		];
	my $genesB = $genesA;
	is_deeply(
			findFused($genesA, $genesB),
			#{ -fusedN => 0, -partialN => 0 },
			{ -full => {}, -partial => {} },
			'Sanity check'
		);

	# Simple case: complete clean fusion
	$genesA = [
			{ s => '+', t => [
					[[30, 14], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440], [2525, 2626]],
					[[30, 14], [1301, 1600], [1631, 1930], [2011, 2310], [2525, 2626]],
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[30, 12], [1301, 1600], [1631, 1930], [2011, 2310]],
					[[30, 12], [1301, 1600], [2011, 2310]],
				] },
			{ s => '+', t => [
					[[20, 14], [2411, 2440], [2525, 2626]]
				] }
		];
	is_deeply(
			findFused($genesA, $genesB),
			# { -fusedN => 1, -partialN => 0 },
			{ -full => { 'g3' => [ 'g3', 'g4' ] }, -partial => {} },
			'Simple clean fusion'
		);

	# Complete fusion with varying CDS boundaries
	$genesA = [
			{ s => '+', t => [
					[[30, 14], [1301, 1597], [1640, 1900], [2041, 2211], [2420, 2452], [2519, 2626]],
					[[30, 14], [1301, 1597], [1640, 1900], [2041, 2211], [2519, 2626]],
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[30, 12], [1301, 1600], [1631, 1930], [2011, 2310]],
					[[30, 12], [1301, 1600], [2011, 2310]],
				] },
			{ s => '+', t => [
					[[20, 14], [2411, 2440], [2525, 2626]]
				] }
		];
	is_deeply(
			findFused($genesA, $genesB),
			#{ -fusedN => 1, -partialN => 0 },
			{ -full => { 'g3' => [ 'g3', 'g4' ] }, -partial => {} },
			'Complete fusion with varying CDS boundaries'
		);

	# Complete fusion with varying UTR boundaries
	$genesA = [
			{ s => '+', t => [
					[[50, 9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440], [2525, 2626]],
					[[50, 9], [1301, 1600], [1631, 1930], [2011, 2310], [2525, 2626]],
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[30, 12], [1301, 1600], [1631, 1930], [2011, 2310]],
					[[30, 12], [1301, 1600], [2011, 2310]],
				] },
			{ s => '+', t => [
					[[20, 14], [2411, 2440], [2525, 2626]]
				] }
		];
	is_deeply(
			findFused($genesA, $genesB),
			# { -fusedN => 1, -partialN => 0 },
			{ -full => { 'g3' => [ 'g3', 'g4' ] }, -partial => {} },
			'Complete fusion with varying UTR boundaries'
		);

	# Complete fusion of three genes
	$genesA = [
			{ s => '+', t => [
					[[30, 14], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440], [2525, 2626]],
					[[30, 14], [1301, 1600], [1631, 1930], [2011, 2310], [2525, 2626]],
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[30, 12], [1301, 1600], [1631, 1930], [2011, 2310]],
					[[30, 12], [1301, 1600], [2011, 2310]],
				] },
			{ s => '+', t => [
					[[20, 14], [2411, 2440]]
				] },
			{ s => '+', t => [
					[[0, 0], [2525, 2626]]
				] }
		];
	is_deeply(
			findFused($genesA, $genesB),
			{ -full => { 'g3' => [ 'g3', 'g4', 'g5' ] }, -partial => {} },
			'Complete fusion of three genes'
		);
	

	# Partial fusion
	$genesA = [
			{ s => '+', t => [
					[[50, 9], [1301, 1600], [1631, 1930], [2011, 2310], [2411, 2440] ],
					[[50, 9], [1301, 1600], [2011, 2310], [2411, 2440]],
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[30, 12], [1301, 1600], [1631, 1930], [2011, 2310]],
					[[30, 12], [1301, 1600], [2011, 2310]],
				] },
			{ s => '+', t => [
					[[20, 14], [2411, 2440], [2525, 2626]]
				] }
		];
	is_deeply(
			findFused($genesA, $genesB),
			# { -fusedN => 0, -partialN => 1 },
			{ -full => { }, -partial => { 'g3' => ['g3', 'g4'] } },
			'Partial fusion'
		);

	# Partial fusion with varying CDS boundaries
	$genesA = [
			{ s => '+', t => [
					[[30, 9], [1301, 1597], [1640, 1900], [2041, 2211], [2420, 2452] ],
					[[30, 9], [1301, 1597], [2041, 2211], [2420, 2452]],
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[30, 12], [1301, 1600], [1631, 1930], [2011, 2310]],
					[[30, 12], [1301, 1600], [2011, 2310]],
				] },
			{ s => '+', t => [
					[[20, 14], [2411, 2440], [2525, 2626]]
				] }
		];
	is_deeply(
			findFused($genesA, $genesB),
			#{ -fusedN => 0, -partialN => 1 },
			{ -full => { }, -partial => { 'g3', => ['g3', 'g4'] } },
			'Complete fusion with varying UTR boundaries'
		);

	# Partial fusion of three genes
	$genesA = [
			{ s => '+', t => [
					[[50, 9], [1631, 1930], [2011, 2310], [2411, 2440] ],
					[[50, 9], [1631, 1930], [2411, 2440]],
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[25, 11], [1301, 1600], [1631, 1930]],
				] },
			{ s => '+', t => [
					[[30, 12], [2011, 2310]],
				] },
			{ s => '+', t => [
					[[20, 14], [2411, 2440], [2525, 2626]]
				] }
		];
	is_deeply(
			findFused($genesA, $genesB),
			# { -fusedN => 0, -partialN => 1 },
			{ -full => { }, -partial => { 'g3' => ['g3', 'g4', 'g5'] } },
			'Partial fusion of three genes'
		);

	# Skip genes with overlapping UTRs but without overlapping CDS
	$genesA = [
			{ s => '+', t => [
					[[30, 9], [1301, 1600], [1631, 1930], [2041, 2310] ],
					[[30, 9], [1301, 1597], [2041, 2310]],
				] }
		];
	$genesB = [
			{ s => '+', t => [ 
					[[1120, 12], [2420, 2452], [2525, 2626], [2701, 2790]],
					[[1120, 12], [2420, 2452], [2701, 2790]],
				] },
		];
	is_deeply(
			findFused($genesA, $genesB),
			# { -fusedN => 0, -partialN => 0 },
			{ -full => { }, -partial => { } },
			'Skip genes with overlapping UTRs but without overlapping CDS'
		);
};



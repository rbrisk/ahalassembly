#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;
use Bio::SeqIO;
use Log::Log4perl qw(:easy);


my $man = 0;
my $help = 0;
my $lineWidth = 100;
my $logLvlStr = "i";
my ($pathAssembly, $pathSites, $pathOut, $pathMap);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"a=s"    => \$pathAssembly,
		"s=s"    => \$pathSites,
		"o=s"    => \$pathOut,
		"m=s"    => \$pathMap,
		"w=i"    => \$lineWidth,
		"v=s"    => \$logLvlStr,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Assembly path is required") unless $pathAssembly;
die("Cutting site path is required") unless $pathSites;
die("Line width must be positive") if ($lineWidth && $lineWidth <= 0);

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});

# Reads the cutting site information from the file. Returns the sites as a hash with scaffolds as
# keys. Values are arrays of two-element arrays that contain start and end coordinates.
sub readSites {
	my ($pathIn) = @_;
	my %sites;
	open(FIN, "<", $pathIn) or die("Unable to read the cutting sites file ($pathIn). $!");
	<FIN>;
	while (<FIN>) {
		chomp;
		my ($scaf, $beg, $end) = split("\t", $_);
		$sites{$scaf} = [] if ( ! defined($sites{$scaf}) );
		push(@{$sites{$scaf}}, [ $beg, $end ]);
	}
	return \%sites;
}

# Determines coordinates of the slices that will be kept
sub findSeqCoords {
	my ($sites, $seqLen) = @_;
	# They should already be sorted but just to be safe...
	my @sortedSites = sort { $a->[0] <=> $b->[0] } @$sites;
	my $beg = 1;
	my $end = 1;
	my @seqSlices;
	for my $s (@sortedSites) {
		$end = $s->[0] - 1;
		push(@seqSlices, [ $beg, $end ]);
		$beg = $s->[1] + 1;
	}
	push(@seqSlices, [ $beg, $seqLen ]);
	return \@seqSlices;
}


INFO("Reading cutting site data");
my $cutSites = readSites($pathSites);

INFO("Cutting scaffolds");
my @seqOut;
my $reader = Bio::SeqIO->new( -file => $pathAssembly, -format => 'fasta' );
while (my $seq = $reader->next_seq()) {
	my $seqId = $seq->display_id;
	if ( defined($cutSites->{$seqId}) ) {
		my $coords = findSeqCoords($cutSites->{$seqId}, $seq->length);
		my $cnt = 0;
		for my $s (@$coords) {
			$cnt++;
			my $subSeq = Bio::Seq->new( 
					-display_id => join("_", $seqId, $s->[0], $s->[1], $cnt),
					-seq => $seq->subseq($s->[0], $s->[1]),
				);
			push(@seqOut, $subSeq);
		}
	} else {
		push(@seqOut, $seq);
	}
}
$reader->close();

DEBUG("Sorting the sequences by length");
@seqOut = sort { $b->length <=> $a->length } @seqOut;

DEBUG("Renaming the sequences");
my @seqMap;
for my $i (0..$#seqOut) {
	my $nameNew = join('_', 'scaffold', $i + 1);
	my $nameOld = $seqOut[$i]->display_id;
	my @F = split('_', $nameOld);
	# We need old scaffold id ($F[1]) for sorting
	if (@F == 2) {
		push(@seqMap, [ $nameOld, 1, $seqOut[$i]->length, 0, $nameNew, $F[1] ]);
	} elsif (@F == 5) {
		push(@seqMap, [ join('_', @F[0,1]), @F[2..4], $nameNew, $F[1] ] );
	} else {
		ERROR("Invalid sequence id ($nameOld)");
	}
	$seqOut[$i]->display_id($nameNew);
}

INFO("Writing the results");
open(my $fh, "> " . ($pathOut || '-')) or die("Unable to write the output to ($pathOut). $!");
my $writer = Bio::SeqIO->new( -fh => $fh, -format => 'fasta');
$writer->width($lineWidth) if $lineWidth;
for my $s (@seqOut) {
	$writer->write_seq($s);
}
$writer->close();

if ($pathMap) {
	# Sort by old scaffold id and start position
	@seqMap = sort { $a->[-1] <=> $b->[-1] || $a->[1] <=> $b->[1] } @seqMap;
	open(FMAP, ">", $pathMap) or die("Unable to write the output to ($pathMap). $!");
	print FMAP join("\t", qw/ Old Beg End Slice New /), "\n";
	for my $row (@seqMap) {
		pop(@$row);
		print FMAP join("\t", @$row), "\n";
	}
	close(FMAP);
}


INFO("Done");



__END__

=head1 NAME

06_cutAssembly.pl - Cuts assembly's scaffolds at the specified cutting points.

=head1 SYNOPSIS

06_cutAssembly.pl -a assembly.fa -s cuttingSites.txt -o output.fa

=head1 OPTIONS

=over 8

=item B<-a>

Genome assembly in fasta format

=item B<-s>

List of sites where cuts will be made. Tab-delimited file with three columns: scaffold id, start
position, stop position. The script will cut at (start - 1) and (stop + 1).

=item B<-o>

Output file in fasta format. The sequences will be sorted by size in descending order.

=item B<-m>

Output file for mapping data. The file will list the old scaffold id, start position, stop position,
sequential slice id, and new scaffold id. If the old scaffold was not sliced, the sequential id is 0.

=item B<-w>

(Optional) Line width in the output fasta file. Default: 100

=item B<-v>

(Optional) Verbosity of the log messages (d, i, w, e, f). Default: i

=back

=head1 DESCRIPTION

Cuts assembly at the provided sites. The site list should include three columns: scaffold name,
start position and end position. The start and end positions are the coordinates of the first and
last N of the segment to be cut out. The script does not make sure that the sequence being cut out
contains only Ns. Therefore, any cutting sites may be specified in principle.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

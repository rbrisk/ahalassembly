#!/usr/bin/env perl

use strict;
use warnings;

#use Data::Dumper;
#$Data::Dumper::Sortkeys = 1;

use Getopt::Long;
use Pod::Usage;
use Log::Log4perl qw(:easy);
use Bio::SeqIO;

my $man = 0;
my $help = 0;
my $logLvlStr = 'i';
my $nThres = 50;
my ($pathSecondary, $pathAssembly, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"s=s"    => \$pathSecondary,
		"a=s"    => \$pathAssembly,
		"o=s"    => \$pathOut,
		"n=i"    => \$nThres,
		"v=s"    => \$logLvlStr,
	) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Secondary path is required") unless $pathSecondary;
die("Assembly path is required") unless $pathAssembly;
die("Output path is required") unless $pathOut;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});


# If the sequence has a stretch of Ns at least as long as the threshold, returns the coordinates of
# that stretch as arrayref. Otherwise, returns [-1, -1].
sub findNCoords {
	my ($seq, $nThres) = @_;

	my @seqArr = split(//, $seq);
	my $n = 0;
	my ($beg, $end) = (-1, -1);
	for (my $i = $#seqArr; $i >= 0; $i--) {
		if ($seqArr[$i] eq 'N') {
			$n++;
		} else {
			$n = 0;
		}
		if ($n >= $nThres) {
			# Position of the first N
			$end = $i + $nThres;
			while ($i >= 0 && $seqArr[$i] eq 'N') {
				$i--;
			}
			# +1 because $i points to the first non-N nucleotide and +1 to make it origin 1 instead of0
			$beg = $i + 2;
			last;
		}
	}

	return [$beg, $end];
}


INFO("Loading assembly sequence");
my %genome;
my $reader = Bio::SeqIO->new( -file => $pathAssembly, -format => 'fasta' );
while (my $seq = $reader->next_seq()) {
	$genome{$seq->display_id()} = $seq;
}
$reader->close();

INFO("Finding slicing sites");
open(FIN, "<", $pathSecondary) or die("Unable to read the secondary file ($pathSecondary). $!");
<FIN>;

my $scaffolds;
my @keys = qw/ hitScaf hitBeg hitEnd geneN refScaf refBeg refEnd isPrim /;
while (<FIN>) {
	chomp;
	my @F = split("\t", $_);
	(my $scafId = $F[0]) =~ s/^scaffold_(\d+)$/$1/;
	$scaffolds->{$scafId} = [] unless defined($scaffolds->{$scafId});
	push(@{$scaffolds->{$scafId}}, {});
	for my $i (0..$#keys) {
		$scaffolds->{$scafId}[-1]{$keys[$i]} = $F[$i];
	}
}
close(FIN);


open(FOUT, ">", $pathOut) or die("Unable to write to the output file ($pathOut). $!");
print FOUT join("\t", qw/ Scaffold Beg End Gap /), "\n";

# Number of sites that may be incorrectly spliced
my $siteN = 0;
# Number of loci we can cut out as they are flanked by Ns
my $sliceSiteN = 0;
for my $scafId ( sort { $a <=> $b } keys(%$scaffolds) ) {
	my $loci = $scaffolds->{$scafId};
	my $scafName = "scaffold_$scafId";
	my $scafSeq = $genome{$scafName};

	# The end coordinate of the last hit on the first stretch of the first primary region
	my $endPrimA = $loci->[0]{'hitEnd'};

	my $slices = [];
	# First and last are primary, so we do not process them
	for (my $i = 1; $i < $#$loci; $i++) {
		# Going from one primary segment to another
		if ($loci->[$i]{'isPrim'}) {
			$endPrimA = $loci->[$i]{'hitEnd'};
			next;
		}
		my $begSec = $loci->[$i]{'hitBeg'};
		my $iSec = $i;
		while ( ! $loci->[$i]{'isPrim'} ) {
			$i++;
		}
		my $endSec = $loci->[$i-1]{'hitEnd'};
		my $begPrimB = $loci->[$i]{'hitBeg'};

		#INFO(join(" ", $endPrimA, $begSec, $endSec, $begPrimB));

		# Cannot slice it out in case of an overlap. We should not count it either.
		next if ($endPrimA >= $begSec || $endSec >= $begPrimB);

		my $relCoordsA = findNCoords($scafSeq->subseq($endPrimA, $begSec), $nThres);
		my $relCoordsB = findNCoords($scafSeq->subseq($endSec, $begPrimB), $nThres);

		#INFO("Rel coords: " . join(" ", @$relCoordsA, @$relCoordsB));

		$siteN++;
		if ($relCoordsA->[0] > 0 && $relCoordsB->[0] > 0) {
			my $gapLen = abs($loci->[$iSec - 1]{'refEnd'} - $loci->[$i]{'refBeg'}) - 1;
			my $beg = $endPrimA + $relCoordsA->[0] - 1;
			my $end = $endSec + $relCoordsB->[1] - 1;
			push(@$slices, [$beg, $end, $gapLen]);
			$sliceSiteN++;
		}
		$endPrimA = $loci->[$i]{'hitEnd'};
	}

	for my $s (@$slices) {
		print FOUT join("\t", $scafName, @$s), "\n";
	}
}
close(FOUT);

INFO("$sliceSiteN out of $siteN sites can be sliced out");


__END__

=head1 NAME

05_findSliceSites.pl - finds non-syntenic sites that can be cut out from long syntenic regions

=head1 SYNOPSIS

05_findSliceSites.pl -s secondary.txt -a assembly.fa -o sliceSites.txt

=head1 OPTIONS

=over 8

=item B<-s>
Path to the file listing the secondary non-syntenic regions. The file should have the following
columns: scaffold, beg, end, gene number, reference scaffold, reference beg, reference end, and
primary flag.

=item B<-a>
Path to the assembly file in fasta format.

=item B<-o>
Path to the output file that will list the scaffold name, beg and end positions, and the distance
between the flanking syntenic genes in the reference genome. Begin and end positions are the
coordinates of the first and last N of the cutting site.

=item B<-n>
Minimum number of Ns required for a slicing site.

=back


=head1 DESCRIPTION

Finds non-syntenic sites that can be cut from syntenic regions. Those sites should contain at least
the specified number of Ns on each side of the non-syntenic locus. The output will include the
scaffold name, the coordinates of the first and the last N of the cutting site, and the distance
between flanking syntenic genes on the reference genome.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

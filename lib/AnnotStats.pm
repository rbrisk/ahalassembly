package AnnotStats;

use strict;
use warnings;

use Bio::DB::SeqFeature::Store;
use Bio::DB::SeqFeature::Store::GFF3Loader;
use POSIX;

use Log::Log4perl qw/ :easy /;

BEGIN {
	use Exporter;
	our ( $VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS );

	$VERSION    = 0.10;
   @ISA        = qw(Exporter);
	@EXPORT     = ();
	@EXPORT_OK  = ();
	%EXPORT_TAGS = ();
}


=head2 new
Arguments : 
Returns   :
Function  : Creates an instance of AnnotStats
=cut

sub new {
	my $class = shift;
	my $self = {
			'db'        => undef,
			'dbLoader'  => undef,
			'mRnaLbl'   => 'transcript',
		};
	my %args = @_;
	for my $option (keys(%$self)) {
		$self->{$option} = $args{"-$option"} if exists $args{"-$option"};
	}
	bless $self, $class;

	$self->{'db'} = Bio::DB::SeqFeature::Store->new( -adaptor => 'memory' );
	$self->{'dbLoader'} = Bio::DB::SeqFeature::Store::GFF3Loader->new( -store => $self->{'db'} );

	return $self;
}



=head2 loadData
Arguments : C<@dataObj> - an array of file paths or file handles pointing to GFF data to be loaded.
Returns   : 
Function  : Initialises the database object by loading the data from the GFF file. The method adds 
gene's primary id to each CDS feature, so that the parent of each CDS could be determined if 
necessary. Note that the module can handle transcripts marked as either 'mRNA' or 'transcript'.
However, only one label can be used within the data set.
=cut

sub loadData {
	my ($self, @dataObj) = @_;
	
	for my $d (@dataObj) {
		$self->{'dbLoader'}->load($d);
	}

	my $db = $self->{'db'};

	my $mRnaN = scalar($db->features(-type => 'mRNA'));
	my $transcriptN = scalar($db->features(-type => 'transcript'));

	if ($mRnaN > 0 && $transcriptN > 0) {
		die("The input file has both mRNA and transcript records but the script can only process " .
				"one label type.");
	} elsif ($mRnaN > 0) {
		$self->{'mRnaLbl'} = 'mRNA';
	}

	# Add gene's id to all CDS
	for my $g ($db->features(-type => 'gene')) {
		my ($gName) = $g->get_tag_values('load_id');
		for my $t ($g->get_SeqFeatures($self->{'mRnaLbl'})) {
			for my $c ($t->get_SeqFeatures('CDS')) {
				$c->add_tag_value('geneId', $g->primary_id);
				$c->add_tag_value('geneName', $gName);
				$db->store($c);
			}
		}
	}
}


=head2 countGenes
Arguments : 
Returns   : The number of genes in the annotation
Function  : Count the number of genes in the annotation
=cut

sub countGenes {
	my ($self) = @_;
	return scalar($self->{'db'}->features(-type => 'gene'));
}


=head2 countTranscripts
Arguments :
Returns   : The number of transcripts in the annotation.
Function  : Count the number of transcripts in the annotation. Transcripts can be marked as either
'mRNA' or 'transcript'. However, only one label is allowed in a data set.
=cut

sub countTranscripts {
	my ($self) = @_;
	return scalar($self->{'db'}->features(-type => $self->{'mRnaLbl'}));
}


=head2 countCds
Arguments :
Returns   : The number of CDS in the annotation
Function  : Count the number of CDS in the annotation.
=cut

sub countCds {
	my ($self) = @_;
	return scalar($self->{'db'}->features(-type => 'CDS'));
}


=head2 countExons
Arguments :
Returns   : The number of exons in the annotation
Function  : Count the number of exons in the annotation
=cut

sub countExons {
	my ($self) = @_;
	return scalar($self->{'db'}->features(-type => 'exon'));
}


=head2 findGeneOverlaps
Arguments :
Returns   : Hashtable that maps gene ids to the list of overlapping genes. 
Function  : Find gene models that overlap. Each pair is reported twice.
=cut

sub findGeneOverlaps {
	my ($self) = @_;
	my %overlaps;
	my $progress = 0;
	my $db = $self->{'db'};
	my @genes = $db->features(-type => 'gene');
	for my $g (@genes) {
		my @overlapGenes = $db->features(
				-type   => 'gene', 
				-seq_id => $g->seq_id,
				-start  => $g->start,
				-stop   => $g->stop,
				-range_type => 'overlaps',
			);
		for my $og (@overlapGenes) {
			if ($g->primary_id ne $og->primary_id) {
				my ($gId) = $g->get_tag_values('load_id');
				my ($ogId) = $og->get_tag_values('load_id');
				$overlaps{$gId} = [] unless defined($overlaps{$gId});
				push(@{$overlaps{$gId}}, $ogId);
			}
		}
		if (++$progress % 5000 == 0) {
			DEBUG("Processed $progress genes");
		}
	}
	# To make the output consistent, sort the genes within the arrays
	while (my ($k, $v) = each(%overlaps)) {
		$overlaps{$k} = [ sort { $a cmp $b } @$v ];
	}
	return \%overlaps;
}


=head2 findCdsOverlaps
Arguments :
Returns   : Hashtable that maps gene ids to the list of genes whose CDS overlap
Function  : Find gene pairs whose CDS overlap. A gene pair that has multiple CDS overlaps is reported
twice: once for the first gene and once for the second.
=cut

sub findCdsOverlaps {
	my ($self) = @_;
	my %overlaps;
	my $progress = 0;

	my $db = $self->{'db'};
	my $mRnaLbl = $self->{'mRnaLbl'};

	my @genes = $db->features(-type => 'gene');
	for my $g (@genes) {
		if (++$progress % 5000 == 0) {
			DEBUG("Processed $progress genes");
		}
		# Skip unless it overlaps with another gene
		my @og = $db->features(
				-type   => 'gene',
				-seq_id => $g->seq_id,
				-start  => $g->start,
				-stop   => $g->stop,
				-range_type => 'overlaps',
			);
		next unless @og >= 2;
		my ($gName) = $g->get_tag_values('load_id');
		my %overlapGenes;
		my @gTranscripts = $g->get_SeqFeatures($mRnaLbl);
		for my $t (@gTranscripts) {
			for my $cds ($t->get_SeqFeatures('CDS')) {
				my @overlapCds = $db->features(
						-type   => 'CDS',
						-seq_id => $cds->seq_id,
						-start  => $cds->start,
						-stop   => $cds->stop,
						-range_type => 'overlaps',
					);
				for my $oCds (@overlapCds) {
					my ($cGeneName) = $oCds->get_tag_values('geneName');
					$overlapGenes{$cGeneName} = 1 unless $cGeneName eq $gName;
				}
			}
		}
		$overlaps{$gName} = [ sort( keys(%overlapGenes) ) ] if keys(%overlapGenes) > 0; 
	}
	# Overlaps are reciprocical
	return \%overlaps
}


=head2 findGenesInUtr
Arguments :
Returns   : Hashtable that maps gene ids to the gene whose UTR completely covers it
Function  : Find genes that are completely contained in a UTR of another gene. Reported genes do not
overlap have CDS overlap. However, the results will also include those rare cases when a gene is
contained by an intron or resides within several introns of another gene.
=cut

sub findGenesInUtr {
	my ($self) = @_;
	my %out;
	
	my $db = $self->{'db'};
	my $mRnaLbl = $self->{'mRnaLbl'};

	my @genes = $db->features(-type => 'gene');
	for my $g (@genes) {
		my @contGenes = $db->features(
				-type   => 'gene',
				-seq_id => $g->seq_id,
				-start  => $g->start,
				-stop   => $g->stop,
				-range_type => 'contains',
			);
		next unless @contGenes >= 2;
		my ($gName) = $g->get_tag_values('load_id');
		for my $contG (@contGenes) {
			next if $contG->primary_id eq $g->primary_id;
			my ($contGName) = $contG->get_tag_values('load_id');
			$out{$contGName} = $gName unless $self->hasCdsOverlap($g, $contG);
		}
	}
	return \%out;
}


=head2
Arguments : C<$g> and C<$cg> are gene are objects of C<SeqFeature> type.
Returns   : C<1> if CDS overlap, C<0> otherwise.
Function  : Private function to determine whether two genes have overlapping CDS
=cut

sub hasCdsOverlap {
	my ($self, $g, $cg) = @_;
	my $hasOverlap = 0;
	my $mRnaLbl = $self->{'mRnaLbl'};
	my $db = $self->{'db'};
	my ($gName) = $g->get_tag_values('load_id');
	TRANS: for my $t ($cg->get_SeqFeatures($mRnaLbl)) {
		for my $c ($t->get_SeqFeatures('CDS')) {
			my @overlapCds = $db->features(
					-type   => 'CDS',
					-seq_id => $c->seq_id,
					-start  => $c->start,
					-stop   => $c->stop,
					-range_type => 'overlaps',
				);
			for my $oc (@overlapCds) {
				my ($ocGName) = $oc->get_tag_values('geneName');
				if ($ocGName eq $gName) {
					$hasOverlap = 1;
					last TRANS;
				}
			}
		}
	}
	return $hasOverlap;
}


=head2
Function  : Creates a histogram of UTR lengths (both 5' and 3'). The histogram may include either all
            transcripts or only the longest transcript. The number of bins and step size are also
            configurable.
Arguments : C<$step> - bin size, the first bin is [0; C<$step>)
            C<$binN> - number of bins; the last bin is catch-all
            C<$useLongestT> - if specified, only the longest transcript of a gene is considered
Returns   : Hash ref with two elements C<-utr5> and C<-utr3>. Each value is an array ref of the size
            C<$binN> that provides counts for the corresponding bin. E.g., if C<$step == 100> and
            C<$binN == 3>, the array ref has counts for the following three bins [0; 100), 
           [100, 200), and [200, inf].
=cut

sub utrLengthHist {
	my ($self, $step, $binN, $useLongestT) = @_;

	my $db = $self->{'db'};
	my $mRnaLbl = $self->{'mRnaLbl'};

	my (@utr3, @utr5);
	for my $i (1..$binN) {
		push(@utr5, 0);
		push(@utr3, 0);
	}

	my @genes = $db->features(-type => 'gene');
	for my $g (@genes) {
		# Elements are array refs [ tLen, utr5, utr3 ]
		my @tData;
		for my $t ($g->get_SeqFeatures(-type => $mRnaLbl)) {
			my @cds = sort { $a->start <=> $b->start } $t->get_SeqFeatures(-type => 'CDS');
			my $tLen = 0;
			map { $tLen += $_->stop - $_->start + 1 } @cds;
			# We do not add 1 because CDS start/stop position should not be counted
			my $utrA = $cds[0]->start - $t->start;
			my $utrB = $t->stop - $cds[-1]->stop;
			my $utr5 = $t->strand > 0 ? $utrA : $utrB;
			my $utr3 = $t->strand > 0 ? $utrB : $utrA;
			# print join(",", $tLen, $t->strand, $utrA, $utrB, $utr5, $utr3), "\n";
			push(@tData, [ $tLen, $utr5, $utr3 ]);
		}
		if ($useLongestT) {
			# Sort by size in descending order
			@tData = sort { $b->[0] <=> $a->[0] } @tData;
			splice(@tData, 1);
		}
		for my $t (@tData) {
			my $i5 = floor($t->[1] / $step);
			$i5 = $#utr5 if $i5 > $#utr5;
			$utr5[$i5]++;
			my $i3 = floor($t->[2] / $step);
			$i3 = $#utr3 if $i3 > $#utr3;
			$utr3[$i3]++;
		}
	}
	return { -utr5 => \@utr5, -utr3 => \@utr3 };
}


=head2
Function  : Retrieves UTR lengths (both 5' and 3') for each gene or transcript. If gene level is
            requested, the values are reported for the longest transcript.
Arguments : C<$useLongestT> - if true, lengths will be reported per transcript rather than per gene.
Returns   : Array ref with each element consisting of four values: gene id, transcript id, 
            5' UTR length and 3' UTR length. 
=cut

sub utrLengths {
	my ($self, $useLongestT) = @_;

	my $db = $self->{'db'};
	my $mRnaLbl = $self->{'mRnaLbl'};

	my @out;

	my @genes = map { $_->[0] } 
			sort { $a->[1] cmp $b->[1] } 
			map { [ $_, $_->get_tag_values('load_id') ] } $db->features(-type => 'gene');
	for my $g (@genes) {
		my ($gName) = $g->get_tag_values('load_id');
		# Elements are array refs [ tLen, utr5, utr3 ]
		# We assume that transcripts were sorted by id in the GFF file because naming schemes may be
		# different and we cannot assume any specific pattern. Overall, it does not matter that much.
		my @tData;
		for my $t (sort { $a->primary_id <=> $b->primary_id } $g->get_SeqFeatures(-type => $mRnaLbl)) {
			my @cds = sort { $a->start <=> $b->start } $t->get_SeqFeatures(-type => 'CDS');
			my $tLen = 0;
			map { $tLen += $_->stop - $_->start + 1 } @cds;
			# We do not add 1 because CDS start/stop position should not be counted
			my $utrA = $cds[0]->start - $t->start;
			my $utrB = $t->stop - $cds[-1]->stop;
			my $utr5 = $t->strand > 0 ? $utrA : $utrB;
			my $utr3 = $t->strand > 0 ? $utrB : $utrA;
			# print join(",", $tLen, $t->strand, $utrA, $utrB, $utr5, $utr3), "\n";
			push(@tData, [ $tLen, $utr5, $utr3 ]);
		}
		if ($useLongestT) {
			# Sort by size in descending order
			@tData = sort { $b->[0] <=> $a->[0] } @tData;
			splice(@tData, 1);
		}
		for my $i (0..$#tData) {
			my $t = $tData[$i];
			push(@out, [$gName, $i + 1, @$t[1,2]]);
		}
	}
	return \@out;
}

1;

__END__

=encoding utf8

=head1 NAME

AnnotStats - Calculates various statistics for the gene annotation in GFF format.

=head1 DESCRIPTION

Calculates various statistics for the gene annotation in GFF format such as gene count, transcript
count, exon count, and CDS count. The module can also find overlapping gene models, genes with
overlapping CDS, and genes contained in another gene's UTR or intron. The input GFF file must be
well formed. For instance, all CDS must have unique IDs.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


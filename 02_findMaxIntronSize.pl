#!/usr/bin/env perl

# Finds the maximum intron size from the records in the specified GFF file.
#
# Author: Roman Briskine, Universität Zürich

use strict;
use warnings;
use IO::Uncompress::AnyUncompress;

if (scalar(@ARGV) != 2) {
	print "Usage: 02_findMaxIntronSize.pl annotation.gff intronN\n";
	exit(1);
}

my $pathGff = shift(@ARGV);
my $n = shift(@ARGV);

my %intronLengths;
my $prevId = "NoId";
my $prevParent = "NoParent";
my $prevEnd = 0;
my $fh = new IO::Uncompress::AnyUncompress($pathGff);
while (<$fh>) {
	next unless /CDS/;
	chomp;
	my ($seqId, $src, $type, $beg, $end, $score, $strand, $phase, $attrStr) = split("\t", $_);
	next unless $type eq "CDS";
	my %attr = ( split(/[=;]/, $attrStr) );
	my $id = $attr{"ID"};
	my $parent = $attr{"Parent"};

	if ($prevParent eq $parent) {
		# We should not count the start and stop positions
		my $len = $beg - $prevEnd - 1;
		$intronLengths{"$prevId - $id"} = $len;
	}
	$prevId = $id;
	$prevParent = $parent;
	$prevEnd = $end;
}

my @sortedKeys = sort { $intronLengths{$b} <=> $intronLengths{$a} } keys(%intronLengths);

print "$n longest introns:\n";
$n--;
for my $i (0..$n) {
	print join(" : ", $sortedKeys[$i], $intronLengths{$sortedKeys[$i]}), "\n";
}


#!/usr/bin/env perl

use strict;
use warnings;

#use Data::Dumper;
#$Data::Dumper::Sortkeys = 1;

use Getopt::Long;
use Pod::Usage;
use Log::Log4perl qw(:easy);

my $man = 0;
my $help = 0;
my $logLvlStr = "i";
my $maxSkipGeneN = 10;
my $gapThres = 1e+5;
my ($pathLoci, $pathPrimary, $pathSecondary);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i=s"    => \$pathLoci,
		"p=s"    => \$pathPrimary,
		"s=s"    => \$pathSecondary,
		"m=i"    => $maxSkipGeneN,
		"g=i"    => \$gapThres,
		"v=s"    => \$logLvlStr,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Loci path is required") unless $pathLoci;
die("Primary output path is required") unless $pathPrimary;
die("Secondary output path is required") unless $pathSecondary;

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});


INFO("Reading loci data");
open(FIN, "<", $pathLoci) or die("Unable to read the input file ($pathLoci). $!");
<FIN>;
my %scaffolds;
while (<FIN>) {
	chomp;
	my @F = split("\t", $_);
	my ($scaf, $beg, $end, $refScaf, $refBeg, $refEnd, $isSynt, $geneDiffN) = @F[0..2, 4..6, 8, 9];
	my $hitN = $F[10];

	if ( ! defined($scaffolds{$scaf}) ) {
		$scaffolds{$scaf} = [ {
				-scaf  => $scaf, 
				-beg   => $beg, 
				-end   => $end, 
				-geneN => 1,
				-refScaf => $refScaf, 
				-refBeg  => $refBeg, 
				-refEnd  => $refEnd,
				-multi   => [ $hitN > 1 ? 1 : 0 ],
			} ];
	} else {
		my $loci = $scaffolds{$scaf};
		my $locus = $loci->[-1];
		#if ($locus->{'-refScaf'} eq $refScaf) {
		if ($isSynt || ($geneDiffN != 0 && abs($geneDiffN) <= $maxSkipGeneN)) {
			$locus->{'-geneN'}++;
			$locus->{'-end'} = $end;
			$locus->{'-refEnd'} = $refEnd;
			push(@{$locus->{'-multi'}}, $hitN > 1 ? 1 : 0);
		} else {
			push(@$loci, {
					-scaf => $scaf,
					-beg   => $beg, 
					-end   => $end, 
					-geneN => 1,
					-refScaf => $refScaf, 
					-refBeg  => $refBeg, 
					-refEnd  => $refEnd,
					-multi   => [ $hitN > 1 ? 1 : 0 ],
				});
		}
	}
}
close(FIN);
#print Dumper(\%scaffolds);


INFO("Processing the data");
open(FPRI, ">", $pathPrimary) or die("Unable to write to the primary file ($pathPrimary). $!");
open(FSEC, ">", $pathSecondary) or die("Unable to write to the secondary file ($pathSecondary). $!");
my @headers = qw/ Scaffold Beg End GeneN RefScaffold RefBeg RefEnd /;
print FPRI join("\t", @headers), "\n";
print FSEC join("\t", @headers, "IsPrimary"), "\n";

my %scafIds;
for my $scafName (keys(%scaffolds)) {
	(my $id = $scafName) =~ s/^scaffold_(.+)$/$1/;
	$scafIds{$scafName} = $id;
}

my @primKeysOut = qw/ -scaf -beg -end -geneN -refScaf -refBeg -refEnd /;
my @secKeysOut = (@primKeysOut, '-isPrim');
my @sortedNames = sort { $scafIds{$a} <=> $scafIds{$b} } keys(%scaffolds);
for my $name (@sortedNames) {
	my $scaf = $scaffolds{$name};
	my $primary = [];
	my $secondary = [];
	
	push(@$primary, $scaf->[0]);
	# The first one is always primary. We need shallow copies as the primary objects are modified.
	push(@$secondary, { %{$scaf->[0]}, '-isPrim' => 1 });
	for (my $i = 1; $i <= $#$scaf; $i++) {
		my $j = $i;
		my $p = $primary->[-1];

		# Is it extendable?
		my $isExt = 0;
		my $pRefLen = $p->{'-refEnd'} - $p->{'-refBeg'};
		# Look ahead for at least $gapThres bp to see if we can extend the current primary locus
		while ($j <= $#$scaf && ($scaf->[$j]{'-beg'} - $p->{'-end'} + 1) < $gapThres) {
			my $s = $scaf->[$j];
			if ($s->{'-refScaf'} eq $p->{'-refScaf'}) {
				# It should have the matching orientation. Note that we are off by 1 here.
				my $sRefLen = $s->{'-refEnd'} - $s->{'-refBeg'};
				my $hasSameOrient = ($pRefLen < 0  ==  $sRefLen < 0);
				# Sometime, the ends may overlap, in which case the difference will be negative. That
				# should still be permissible as gap extension into the opposite direction is unlikely.
				if ($hasSameOrient && abs($s->{'-refBeg'} - $p->{'-refEnd'}) + 1 <= $gapThres) {
					$isExt = 1;
					last;
				}
			}
			$j++;
		}
		# If it is extendable, push the genes from the gap into the secondary list
		if ($isExt) {
			$p->{'-end'} = $scaf->[$j]{'-end'};
			$p->{'-refEnd'} = $scaf->[$j]{'-refEnd'};
			$p->{'-geneN'} += $scaf->[$j]{'-geneN'};
			my $gap = [];
			while ($i < $j) {
				$scaf->[$i]{'-isPrim'} = 0;
				push(@$gap, $scaf->[$i]);
				$i++;
			}
			# We don't really care if the gap is sliced out as a single contig or multiple ones
			push(@$secondary, @$gap);
			# To figure out the boundaries, we need to add the j-th locus to secondary
			push(@$secondary, { %{$scaf->[$j]}, '-isPrim' => 1 });
		} else {
			push(@$primary, $scaf->[$i]);
			push(@$secondary, { %{$scaf->[$i]}, '-isPrim' => 1 });
		}
	}
	
	for my $p (@$primary) {
		print FPRI join("\t", @$p{@primKeysOut}), "\n";
	}
	for my $s (@$secondary) {
		print FSEC join("\t", @$s{@secKeysOut}), "\n";
	}
}	

close(FPRI);
close(FSEC);

INFO("Done");

__END__

=head1 NAME

04_compressHits.pl - compresses the hits into primary and secondary syntenic regions

=head1 SYNOPSIS

04_compressHits.pl -i loci.txt -p primary.txt -s secondary.txt

=head1 OPTIONS

=over 8

=item B<-i>

Input file that provides hit information. The file should have the following columns: hit scaffold,
hit beg, hit end, gene, gene scaffold, gene beg, gene end, flag indicating whether the hit is first
on the scaffold, flag indicating whether the region is syntenic (-1, 0, 1 for reverse, no synteny and
direct synteny respectively), number of genes between two hits that are not syntenic but reside on
the same scaffold, and the number of hits within the region.

=item B<-p>

Output file for the primary syntenic regions.

=item B<-s>

Output file for the secondary syntenic regions. This file also contains portions of the primary
syntenic regions, so that the exact boundary could be determined.

=item B<-m>

Maximum number of genes that can be skipped within a primary syntenic region. If the number of genes
exceeds this number, the region is reported as two primary syntenic regions.

=item B<-g>

Maximum gap allowed between two syntenic hits within a primary syntenic region and within the
corresponding genes on the reference genome.

=back

=head1 DESCRIPTION

Compresses contiguous hits into primary and secondary syntenic regions. Primary syntenic regions are
those that are not inserted into larger syntenic regions. A primary region may be very short (1
gene) if it is located at the beginning or the end of a scaffold. It can also be short if it lies
between two long primary syntenic regions. Gaps are allowed within primary syntenic regions but
should be limited to fewer than the specified number of genes. Gap's length should not exceed the
specified threshold either.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut

#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;


my $man = 0;
my $help = 0;
my $minCov = 0.9;
my $mismatchPenalty = 2;
my $gapPenalty = 3;
my ($pathIn, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"i=s"    => \$pathIn,
		"o=s"    => \$pathOut,
		"c=f"    => \$minCov,
		"m=f"    => \$mismatchPenalty,
		"g=f"    => \$gapPenalty,
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Input path is required") unless $pathIn;
die("Output path is required") unless $pathOut;
die("Coverage threshold must be between 0 and 1") unless ($minCov >= 0 && $minCov <= 1);

open(FIN, "<", $pathIn) or die("Unable to read the input file ($pathIn). $!");
open(FOUT, ">", $pathOut) or die("Unable to write to the output file ($pathOut). $!");

# Table header has 5 lines
for my $i (1..5) {
	my $header = <FIN>;
	print FOUT $header;
}

print "Finding best hits\n";
my %bestHits;
while (<FIN>) {
	chomp;
	my @fields = split(/\t/, $_);
	my ($qryLen, $qryBeg, $qryEnd) = @fields[10..12];
	my $targetCov = ($qryEnd - $qryBeg + 1) / $qryLen;
	next unless $targetCov >= $minCov;

	# Score = matchN - mismatchPenalty * mismatch - gapPenalty * gapN
	my ($matchN, $mismatchN, $gapN, $gapLen, $geneId) = @fields[0,1,6,7,9];
	my $score = $matchN - $mismatchPenalty * $mismatchN - $gapPenalty * $gapN;

	my ($tScaf, $tBeg) = @fields[13,15];
	$tScaf =~ s/^scaffold_//;
	my $exists = defined($bestHits{$geneId});
	my $biggerScore = $exists && $score >  $bestHits{$geneId}->{'-score'};
	my $smallerGap = $exists && 
			$score == $bestHits{$geneId}->{'-score'} &&
			$gapLen < $bestHits{$geneId}->{'-gapLen'};

	if ( ! $exists || $biggerScore || $smallerGap ) {
		$bestHits{$geneId} = { 
				-rec   => $_, 
				-score => $score, 
				-tScaf => $tScaf, 
				-tBeg  => $tBeg,
				-gapLen=> $gapLen,
			};
	}
}

# Sort keys by target position
print "Sorting records by target position\n";
my @sortedKeys = sort { 
		$bestHits{$a}->{'-tScaf'} <=> $bestHits{$b}->{'-tScaf'} || 
		$bestHits{$a}->{'-tBeg'}  <=> $bestHits{$b}->{'-tBeg'} } keys(%bestHits);

for my $k (@sortedKeys) {
	print FOUT $bestHits{$k}->{'-rec'}, "\n";
}

print "Done\n";


__END__

=head1 NAME

02_selectBest.pl - Selects best hits from the blat output file and sorts them by target position.

=head1 SYNOPSIS

02_selectBest.pl -i hitsAll.psl -o hitsBest.psl -c 0.85

=head1 OPTIONS

=over 8

=item B<-i>

Input file in PSL (standard BLAT output) format

=item B<-o>

Output file in PSL (standard BLAT output) format

=item B<-c>

(Optional) Minimum query coverage (0 < c < 1) required for the hit to be selected. Default: 0.85

=item B<-m>

(Optional) Mismatch penalty for score calculation. Default: 2

=item B<-g> 

(Optional) Gap penalty for score calculation. Default: 3

=back

=head1 DESCRIPTION

Selects best hits from the BLAT output file in standard PSL format and sorts them by target
position. The script determines the best hit based on the score calculated as

matchN - mismatchPenalty * mismatchN - gapPenalty * gapN

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


#!/usr/bin/env Rscript

# Extends the variant file with nearest TE information. If variant's position is within a TE,
# the TE.Dist is set to 0. Otherwise, it is the distance between the variant and its nearest TE. If
# the variant containing sequence does not have any annotated TE, the variant's distance is set to
# NA. The script will not scale well for assemblies with long scaffolds. To improve performance, the
# script discards information about TEs located farther than the distance threshold (500 bp by
# default).

require(optparse, quietly=T)
require(futile.logger, quietly=T)
require(stringr, quietly=T)
require(readr, quietly=T)
require(dplyr, quietly=T, warn.conflicts=F)

optList <- list(
		make_option(c("-v", "--variants"), type = "character",
				help = "Variants file (table constructed by GATK)"),
		make_option(c("-t", "--te"), type = "character",
				help = "TE data in GFF format"),
		make_option(c("-o", "--out"), type = "character",
				help = "Path to the output file name."),
		make_option("--maxDist", type = "integer", default = 500,
				help = "(Optional) TEs located farther away than this value will be ignored. Default: %default"),
		make_option(c("-l", "--verbosity"), type = "integer", default = INFO,
				help = "Logging verbosity (t=9,d=8,i=6,w=4,e=2,f=1). Default: %default")
	)

opt <- parse_args( OptionParser(
		usage = "Usage: %prog [options]",
		option_list = optList,
		description = "Extends the variant file with two columns: TE.Dist and TE.Family")
	)

gffColNames <- c("SeqId", "Source", "Type", "Beg", "End", "Score", "Strand", "Frame", "Attrib")
devnull <- flog.threshold(opt$verbosity)

flog.info("Reading the variants file")
variants <- read_tsv(opt$variants)

flog.info("Loading TE")
te <- read_tsv(opt$te, skip = 1, col_names = gffColNames) %>%
		mutate(TE.Family = sub("^.*family=([^;]+).*", "\\1", Attrib, perl=T)) %>%
		select(SeqId, Beg, End, TE.Family)
		
flog.info("Finding nearest TE")
nearestTe <- left_join(select(variants, CHROM, POS), te, by=c("CHROM" = "SeqId")) %>% 
		mutate(TE.Dist = ifelse(
				POS >= Beg & POS <= End, 
				0, 
				pmin(abs(POS - Beg), abs(POS - End))
			)) %>% 
		filter(TE.Dist <= opt$maxDist) %>%
		group_by(CHROM, POS) %>% 
		arrange(TE.Dist) %>% 
		filter(!is.na(TE.Dist), min_rank(TE.Dist) == 1) %>%
		select(-Beg, -End)

flog.info("Joining the data sets")
variants <- left_join(variants, nearestTe, by = c("CHROM" = "CHROM", "POS" = "POS"))

flog.info("Writing output")
write_tsv(variants, opt$out)

flog.info("Done")


package ch.uzh.shimizulab.akam.variants

import org.broadinstitute.gatk.queue.QScript
import org.broadinstitute.gatk.queue.extensions.gatk._
import org.broadinstitute.gatk.queue.extensions.picard._
import org.broadinstitute.gatk.queue.function.JavaCommandLineFunction
import org.broadinstitute.gatk.queue.util.Logging

import htsjdk.samtools.ValidationStringency
import htsjdk.samtools.SAMFileHeader.SortOrder

import scala.util.matching._



class CallVariants extends QScript with Logging {

	qs =>

	@Input(doc="The reference file in FASTA format", 
			shortName="R", fullName="reference", required=true)
	var pathRef: File = _

	@Input(doc="File with reads. May be archived with gzip", 
			shortName="reads", fullName="reads", required=true)
	var pathReads: List[File] = Nil

	@Input(doc="File with mates. May be archived with gzip",
			shortName="mates", fullName="mates", required=false)
	var pathMates: List[File] = Nil

	@Argument(doc="Number of data threads to use for jobs that support -nt. This does not apply to " +
			"jobs that support scattering.", shortName="t", fullName="threadN", required=false)
	var threadN: Int = 8

	@Argument(doc="Number of CPU threads to use for jobs that support -nct. Scatter jobs will get " +
			"ct / sj threads each", shortName="ct", fullName="cpuThreadN", required=false)
	var cpuThreadN: Int = 8

	@Argument(doc="Number of scatter jobs to generate", 
			shortName="sj", fullName="scatterN", required=false)
	var scatterN: Int = 8

	@Argument(doc="Maximum amount of memory to use in GB",
			shortName="maxMem", fullName="maxMemory", required=false)
	var maxMem: Int = 32

	@Argument(doc="Maximum records in RAM", 
			shortName="maxRec", fullName="maxRecordsInRam", required=false)
	var maxRecordsInRam: Int = 1e+7.toInt

	@Argument(doc="Maximum file handles for MarkDuplicates",
			shortName="maxFh", fullName="maxFileHandleN", required=false)
	var maxFileHandleN: Int = 300

	@Argument(doc="Optional list of filter names", 
			fullName="filterName", shortName="filterName", required=false)
	var filterNames: List[String] = Nil

	@Argument(doc="Optional list of filter expressions", 
			shortName="filter", fullName="filter", required=false)
	var filterExpressions: List[String] = Nil

	@Argument(doc="Optional list of genotype filter names", fullName="gFilterName",
			shortName="gFilterName", required=false)
	var gFilterNames: List[String] = Nil

	@Argument(doc="Optional list of genotype filer expressions", fullName="gFilterExpression",
			shortName="gFilter", required=false)
	var gFilterExpressions: List[String] = Nil

	@Argument(doc="Sample name", 
			shortName="sn", fullName="sampleName", required=false)
	var sampleName: String = "S1"


	trait commonArgs extends CommandLineGATK {
		this.reference_sequence = qs.pathRef
	}

	def ensurePositive(x: Int): Int = if (x > 0) x else 1;

	def script() {
		val prefixPtrn = """^(.+)[-_]R[12]\..+$""".r
		val cpuPerThread = ensurePositive((qs.cpuThreadN / qs.scatterN).toInt)
		val memPerThread = (qs.maxMem / qs.cpuThreadN).toInt
		val bamList: List[File] = for (pair <- pathReads.zipWithIndex) yield {
			val reads = pair._1
			val mates = qs.pathMates(pair._2)
			val prefix = prefixPtrn.replaceFirstIn(pair._1.getName, "$1")
			val sn = qs.sampleName
			val rg = List(
					"@RG", s"ID:$prefix", "PL:Illumina", s"PU:$prefix", s"LB:$prefix", s"SM:$sn"
				).mkString("""\t""")

			val bwaAligner = new BwaAligner {
				this.analysisName = "BwaAligner"
				this.pathRef = qs.pathRef
				this.pathReads = reads
				this.pathMates = Some(mates)
				this.memoryLimit = qs.maxMem
				this.nCoresRequest = qs.threadN
				this.readGroup = Some(rg)
				this.pathOut = s"01_aligned_$prefix.bam"
				this.isIntermediate = true
			}

			// Fix mates
			val fixMates = new FixMateInformation {
				this.isIntermediate = true
				this.input = bwaAligner.pathOut
				this.out = s"02_matesFixed_$prefix.bam"
				this.sortOrder = SortOrder.coordinate
				this.createIndex = Some(true)
				this.validationStringency = ValidationStringency.LENIENT
				this.maxRecordsInRam = qs.maxRecordsInRam
				this.javaMemoryLimit = qs.maxMem
			}

			// Mark duplicates
			val markDups = new MarkDuplicates {
				this.isIntermediate = true
				this.input :+= fixMates.out
				this.output = s"03_dupMarked_$prefix.bam"
				// Previously, it tried to get output value before it was frozen. 
				// Not sure if it is still true.
				this.metrics = s"03_dupMarked_$prefix.metrics"
				this.REMOVE_DUPLICATES = true
				this.MAX_FILE_HANDLES_FOR_READ_ENDS_MAP = qs.maxFileHandleN
				this.javaMemoryLimit = qs.maxMem
			}

			add(bwaAligner, fixMates, markDups)

			markDups.output
		}
	


		// Create target intervals and realign
		val rtc = new RealignerTargetCreator with commonArgs {
			this.isIntermediate = true
			this.num_threads = qs.threadN
			this.memoryLimit = qs.maxMem
			this.input_file = bamList
			this.out = "04_realigned.intervals"
		}

		val realigner = new IndelRealigner with commonArgs {
			this.isIntermediate = false
			this.scatterCount = qs.scatterN
			this.num_cpu_threads_per_data_thread = 1
			this.num_threads = 1
			this.memoryLimit = memPerThread
			this.input_file = bamList
			this.targetIntervals = rtc.out
			this.out = "05_realigned.bam"
		}

		// Count covered bases (needed to calculate heterozygosity level)
		val countLoci = new CountLoci with commonArgs {
			this.isIntermediate = false
			this.num_threads = qs.threadN
			this.input_file = List(realigner.out)
			this.out = "05_coveredBases.txt"
		}

		add(rtc, realigner, countLoci)

		// Call variants
		val haplotypeCaller = new HaplotypeCaller with commonArgs {
			this.isIntermediate = false
			this.scatterCount = qs.scatterN
			this.num_cpu_threads_per_data_thread = cpuPerThread
			this.memoryLimit = memPerThread
			this.input_file :+= realigner.out
			this.out = "raw.vcf.gz"
		}
		add(haplotypeCaller)

		// Evaluate unfiltered variants
		val evalUnfiltered = new VariantEval with commonArgs {
			this.isIntermediate = false
			this.memoryLimit = qs.maxMem
			this.eval :+= haplotypeCaller.out
			this.out = "raw_report.txt"
		}
		add(evalUnfiltered)

		// Filter variants
		val variantFilter = new VariantFiltration with commonArgs {
			this.isIntermediate = false
			this.variant = haplotypeCaller.out
			this.out = "filtered.vcf.gz"
			this.filterName = qs.filterNames
			this.filterExpression = qs.filterExpressions
		}

		// Evaluate filtered variants
		val evalFiltered = new VariantEval with commonArgs {
			this.isIntermediate = false
			this.memoryLimit = qs.maxMem
			this.eval :+= variantFilter.out
			this.out = "filtered_report.txt"
		}
		if (filterNames.size > 0) {
			add(variantFilter, evalFiltered)
		}

		// Convert to table
		val vtt = new VariantsToTable with commonArgs {
			this.isIntermediate = true
			this.fields ++= List("CHROM", "POS", "TYPE", "QUAL", "QD", "MQ", "FS", "SOR", "MQRankSum", 
					"ReadPosRankSum")
			this.genotypeFields ++= List("AD", "GQ")
			this.allowMissingData = true
		}
		if (filterNames.size > 0) {
			vtt.out = "filtered.txt"
			vtt.variant :+= variantFilter.out
		} else {
			vtt.out = "raw.txt"
			vtt.variant :+= haplotypeCaller.out
		}
		add(vtt)

		val vttCompress = new FileCompressor {
			this.isIntermediate = false
			this.nCoresRequest = qs.threadN
			this.pathIn = vtt.out
			this.pathOut = vtt.out + ".xz"
		}
		add(vttCompress)
	}



	class BwaAligner extends CommandLineFunction {
		@Input(doc="Indexed reference file in FASTA format (may be compressed)")
		var pathRef: File = _

		@Input(doc="List of files with first mates (may be compressed)")
		var pathReads: File = _

		@Input(doc="List of file with second mates (may be compressed). Must be in the same order " +
				"as first mates", required=false)
		var pathMates: Option[File] = _

		@Output(doc="Output in bam format sorted by query")
		var pathOut: File = _

		@Argument(doc="Read group information", required=false)
		var readGroup: Option[String] = None

		// -M is required for picard compatibility. -v 2 in principle should reduce verbosity.
		def commandLine = "bwa mem " + 
				"-M -v 2" +
				required("-t", nCoresRequest, escape=false) + 
				optional("-R", readGroup) +
				required(pathRef) +
				required(pathReads) +
				optional(pathMates) +
				"| samtools sort -T tmpSort -n " +
				optional("-@", nCoresRequest, escape=false) +
				optional("-m", memoryLimit.toInt + "G", escape=false) +
				required("-o", pathOut)
	}



	class FixMateInformation extends JavaCommandLineFunction with PicardBamFunction  {
		analysisName = "FixMateInformation"
		javaMainClass = "picard.sam.FixMateInformation"

		@Input(doc = "The input SAM or BAM file to analyze. Must be coordinate sorted.", 
				required = true)
		var input: File = _

		@Output(doc="The output file to write fixed records to", required = true)
		var out: File = _

		@Output(doc="The output bam index", required = false)
		var outIndex: File = _

		@Argument(doc="If true, Adds the mate CIGAR tag (MC)", required = false)
		var addMateCigar: Boolean = false

		override def freezeFieldValues() {
			super.freezeFieldValues()
			if (outIndex == null && out != null) {
				outIndex = new File(out.getName.stripSuffix(".bam") + ".bai")
			}
		}

		override def inputBams = Seq(input)
		override def outputBam = out
		this.createIndex = Some(true)

		override def commandLine = super.commandLine +
				conditional(addMateCigar, "ADD_MATE_CIGAR=true")
	}


	class FileCompressor extends CommandLineFunction {
		@Input(doc="Input file")
		var pathIn: File = _

		@Output(doc="Output file")
		var pathOut: File = _

		def commandLine = {
			val extPtrn = """^.+\.([^.]+)$""".r
			val ext: String = extPtrn.replaceFirstIn(pathIn, "$1")
			val cmd = ext match {
				case "gz"  => "gzip"
				case "bz2" => "bzip2"
				case _     => "xz" + optional("-T", nCoresRequest, escape=false)
			}

			cmd + " -c " + required(pathIn) + " > " + required(pathOut)
		}
	}

}




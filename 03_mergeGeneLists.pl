#!/usr/bin/env perl

use strict;
use warnings;

use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

use IO::Uncompress::AnyUncompress;
use Log::Log4perl qw/ :easy /;


my $man = 0;
my $help = 0;
my $logLvlStr = "i";
my $minCov = 0.9;
my $mismatchPenalty = 2;
my $minOverlap = 100;
my $gapPenalty = 3;
my $randSeed = 16;
my ($pathBlat, $pathGff, $pathOut);

GetOptions(
		"h|help" => \$help,
		"man"    => \$man,
		"b=s"    => \$pathBlat,
		"g=s"    => \$pathGff,
		"o=s"    => \$pathOut,
		"c=f"    => \$minCov,
		"m=f"    => \$mismatchPenalty,
		"p=f"    => \$gapPenalty,
		"s=f"    => \$minOverlap,
		"v=s"    => \$logLvlStr, 
) or pod2usage(2);

pod2usage(1) if $help;
pod2usage(-verbose => 2) if $man;

die("Path to the BLAT output is required") unless $pathBlat;
die("Path to the annotation file is required") unless $pathGff;
die("Output path is required") unless $pathOut;
die("Coverage threshold must be between 0 and 1") unless ($minCov >= 0 && $minCov <= 1);

my %logLvlDict = ("d" => $DEBUG, "i" => $INFO, "w" => $WARN, "e" => $ERROR, "f" => $FATAL);
$logLvlStr = "i" unless exists($logLvlDict{$logLvlStr});
Log::Log4perl->easy_init({level => $logLvlDict{$logLvlStr}, layout => "%d %p %m%n"});


# Reads the gene information (CDS) from the specified file
sub readAnnotation {
	my $pathGff = shift;

	DEBUG("Reading the annotation file ($pathGff)");
	# Simulated doubly-linked list. Although we only need to know the location of the next gene.
	my %srcGenes;
	my $fhGff = new IO::Uncompress::AnyUncompress($pathGff);
	while (<$fhGff>) {
		next unless /\tgene\t/;
		chomp;
		my ($seqId, $src, $type, $beg, $end, $score, $strand, $phase, $attrStr) = split("\t", $_);
		next unless $type eq 'gene';
		
		my %attr = split(/[=;]/, $attrStr);
		my $name = $attr{"Name"};
	
		# The last attribute is set to 1 whenever gene's syntenic hit is found
		$srcGenes{$name} = { 
				-gene => $name, 
				-seq  => $seqId, 
				-beg  => $beg, 
				-end  => $end, 
				-prev => "", 
				-next => "",
				-synt => 0,
			};
	}
	close($fhGff);
	return \%srcGenes;
}


# Links the neighboring genes in the provided hash
sub linkAnnGenes {
	my $srcGenes = shift;

	# Add the locations of the previous and the next genes
	DEBUG("Sorting the genes");
	my @sortedGenes = sort { 
			$srcGenes->{$a}{'-seq'} cmp $srcGenes->{$b}{'-seq'} ||
			$srcGenes->{$a}{'-beg'} <=> $srcGenes->{$b}{'-beg'} ||
			$srcGenes->{$a}{'-end'} <=> $srcGenes->{$b}{'-end'} } keys(%$srcGenes);
	
	DEBUG("Linking the genes");
	for my $i (0..$#sortedGenes) {
		my $curGene = $sortedGenes[$i];
		# Set the previous gene name
		if ($i > 0) {
			my $prevGene = $sortedGenes[$i - 1];
			if ($srcGenes->{$prevGene}{'-seq'} eq $srcGenes->{$curGene}{'-seq'}) {
				$srcGenes->{$curGene}{'-prev'} = $prevGene;
			}
		}
		# Set the next gene name
		if ($i < $#sortedGenes) {
			my $nextGene = $sortedGenes[$i + 1];
			if ($srcGenes->{$curGene}{'-seq'} eq $srcGenes->{$nextGene}{'-seq'}) {
				$srcGenes->{$curGene}{'-next'} = $nextGene;
			}
		}
	}
}	



# Reads the BLAT output filtering out the hits that have insufficient query coverage
sub readBlatResults {
	my ($pathBlat, $minCov, $mismatchPenalty, $gapPenalty) = @_;
	my @hits;

	open(FBLT, "<", $pathBlat) or die("Unable to read the BLAT file ($pathBlat). $!");

	# Skip the header
	for my $i (1..5) {
		<FBLT>;
	}

	while (<FBLT>) {
		chomp;
		my @F = split(/\t/, $_);
		my ($geneId, $qryLen, $qryBeg, $qryEnd, $tScaf, $tBeg, $tEnd) = @F[9..13,15,16];
		$geneId =~ s/^([^|]+).*$/$1/;
		(my $tScafId = $tScaf) =~ s/^scaffold_//;

		my $targetCov = ($qryEnd - $qryBeg + 1) / $qryLen;
		next unless $targetCov >= $minCov;

		# Score = matchN - mismatchPenalty * mismatch - gapPenalty * gapN
		my ($matchN, $mismatchN, $gapN, $gapLen) = @F[0,1,6,7];
		my $score = $matchN - $mismatchPenalty * $mismatchN - $gapPenalty * $gapN;
		push(@hits, {
				-gene   => $geneId,
				-scaf   => $tScaf,
				-scafId => $tScafId,
				-beg    => $tBeg,
				-end    => $tEnd,
				-score  => $score,
				-gapLen => $gapLen,
				-synt   => 0,
			});
	}
	close(FBLT);

	@hits = sort { 
			$a->{'-scafId'} <=> $b->{'-scafId'} || 
			$a->{'-beg'} <=> $b->{'-beg'} ||
			$a->{'-end'} <=> $b->{'-end'}
		} @hits;

	return \@hits;
}
	


# Compresses the overlapping hits into an array ref within the array. Non-overlapping entries are
# also placed into individual (single record) array refs. Also, add locus attributes for each entry.
sub compressHits {
	my ($hitsIn, $minOverlap) = @_;
	my @hitsOut;
	
	push(@hitsOut, [ $hitsIn->[0] ]);
	for my $i (1..$#$hitsIn) {
		my $curr = $hitsIn->[$i];
		my $prev = $hitsIn->[$i - 1];
		my $hasOverlap = 0;
		# Check for overlap only when they are on the same scaffold
		if ($curr->{'-scaf'} eq $prev->{'-scaf'}) {
			# Compare to each from the overlapping hits (if more than one)
			for my $h (@{$hitsOut[-1]}) {
				$hasOverlap = 1 if $h->{'-end'} - $curr->{'-beg'} >= $minOverlap;
			}
		}
		# If it overlaps with the previous hit, put them into a single array ref. Otherwise, add a new
		# array ref.
		if ($hasOverlap) {
			push(@{$hitsOut[-1]}, $curr);
		} else {
			push(@hitsOut, [$curr]);
		}
	}

	# Add locus attributes and resort the hits by score
	for my $h (@hitsOut) {
		my @sortedHits = sort { 
				$b->{'-score'} <=> $a->{'-score'} || 
				$a->{'-gapLen'} <=> $b->{'-gapLen'} ||
				rand() <=> 0.5 
			} @$h;
		# -best is the index of the best hit within the @sortedHits
		# -first indicates whether this is the first gene on the scaffold
		# -synt synteny of the previous and the current hits (negative when current gene is upstream)
		# -geneN the number of genes between the previous hit and the current best hit, sign indicates
		# direction (negative when the current gene is upstream). If genes are on different scaffolds,
		# the value is 0.
		# -hits Overlapping hits at the current locus for genes that are not syntenic elsewhere
		$h = {
			-best  => -1,
			-first => 0,
			-hits  => \@sortedHits,
			-geneN => 0,
			-synt  => 0,
		};
	}
	
	return \@hitsOut;
}


# Some secondary hits span regions that include hits from multiple genes. Out of those genes, only
# the first is selected as the syntenic hit. That followed by a gap (geneN may be as large as 10).
# This can be avoided if we split (decompress) loci that no longer overlap after the secondary hit
# was removed.
sub decompressHits {
	my ($loci, $minOverlap) = @_;
	my @lociOut;

	for my $locus (@$loci) {
		my $hitN = scalar(@{$locus->{'-hits'}});
		# Copy the locus if it has only one hit
		push(@lociOut, $locus) if $hitN == 1;
		next unless $hitN > 1;

		# Resort by coordinates and check if they still overlap
		my @sortedHits = sort { 
				$a->{'-beg'} <=> $b->{'-beg'} || 
				$a->{'-end'} <=> $b->{'-end'} } @{$locus->{'-hits'}};
		my $hasGaps = 0;
		#if ($sortedHits[0]->{'-beg'} == 370300) {
		#	die("Doh!");
		#}
		for my $i (1..$#sortedHits) {
			if ($sortedHits[$i-1]->{'-end'} - $sortedHits[$i]->{'-beg'} < $minOverlap) {
				$hasGaps = 1;
				last;
			}
		}
		
		# Copy the locus if it does not have any gaps. Otherwise, split it and reset all the
		# attributes
		if (!$hasGaps) {
			push(@lociOut, $locus);
		} else {
			my $newLocusN = 0;
			for my $i (0..$#sortedHits) {
				if ($i == 0 || $sortedHits[$i-1]->{'-end'} - $sortedHits[$i]->{'-beg'} < $minOverlap) {
					my $newLocus = {
							-best  => -1,
							-first => 0,
							-hits  => [],
							-geneN => 0,
							-synt  => 0
						};
					push(@lociOut, $newLocus);
					$newLocusN++;
				}
				push(@{$lociOut[-1]->{'-hits'}}, $sortedHits[$i]);
			}
			# Resort the hits in the new loci by score
			for (my $i = $#lociOut - $newLocusN + 1; $i < @lociOut; $i++) {
				$lociOut[-1]->{'-hits'} = [ sort {
						$a->{'-score'} <=> $b->{'-score'} ||
						$a->{'-gapLen'} <=> $b->{'-gapLen'} ||
						rand() <=> 0.5 
					} @{$lociOut[-1]->{'-hits'}} ];
			}
		}
	}

	my $oldN = scalar(@$loci);
	# Note that you cannot do $loci = \@lociOut;
	splice(@$loci);
	push(@$loci, @lociOut);
	
	return scalar(@$loci) - $oldN;
}




# Removes hits from genes that are syntenic elsewhere
sub removeSyntenic {
	my ($loci, $srcGenes) = @_;

	for my $locus (@$loci) {
		my $locusHits = [];
		my $oldHitN = scalar(@{$locus->{'-hits'}});
		for my $h (@{$locus->{'-hits'}}) {
			my $gene = $h->{'-gene'};
			if ($h->{'-synt'} || ! $srcGenes->{$gene}{'-synt'}) {
				push(@$locusHits, $h);
			}
		}
		# Update the locus record
		$locus->{'-hits'} = $locusHits;

		# If the locus is syntenic and we removed some items, update the index
		# Continue with the next entry
		if ( $locus->{'-synt'} ) {
			if ( scalar(@$locusHits) != $oldHitN ) {
				my $idx = -1;
				do { $idx++ } until ($locusHits->[$idx]{'-synt'} || $idx > $#$locusHits);
				$locus->{'-best'} = $idx;
			}
		}
	}

	# Remove loci that have no hits left
	my $oldN = scalar(@$loci);
	for (my $i = $#$loci; $i >= 0; $i--) {
		my $locus = $loci->[$i];
		splice($loci, $i, 1) if @{$locus->{'-hits'}} == 0;
	}
	return scalar(@$loci) - $oldN;
}




# Loops through all loci and updates the best hit info Returns the net change in locus count
sub processLoci {
	my ($loci, $srcGenes) = @_;

	my $oldLociN = scalar(@$loci);


	for my $i (0..$#$loci) {
		my $locus = $loci->[$i];
		my $oldHitN = scalar(@{$locus->{'-hits'}});
		my $locusHits = $locus->{'-hits'};

		# If it is the first gene on the scaffold, select the best hit based on the score (first hit)
		if ($i == 0 || $loci->[$i-1]{'-hits'}[0]{'-scaf'} ne $locusHits->[0]{'-scaf'}) {
			my $bestHit = $locusHits->[0];
			$bestHit->{'-synt'} = 1;
			my $bestGene = $bestHit->{'-gene'};
			$srcGenes->{$bestGene}{'-synt'} = 1;
			my %attr = (
					-best  => 0,
					-first => 1,
					-geneN => 0,
					-synt  => 1,
				);
			map { $locus->{$_} = $attr{$_} } keys(%attr);
			next;
		}

		my $prevLocus = $loci->[$i-1];
		my $prevHit = $prevLocus->{'-hits'}[$prevLocus->{'-best'}];
		my $prevGene = $srcGenes->{$prevHit->{'-gene'}};

		# Index of the hit within the locus that matches prevGene's previous gene or next gene
		# (reference coordinates)
		my $prevMatch = -1;
		my $nextMatch = -1;
	
		# This works because a gene can only have a single hit within a locus (hopefully)
		for my $i (0..$#$locusHits) {
			if ($prevGene->{'-next'} eq $locusHits->[$i]{'-gene'}) {
				$nextMatch = $i;
			} elsif ($prevGene->{'-prev'} eq $locusHits->[$i]{'-gene'}) {
				$prevMatch = $i;
			}
		}
	
		my $bestMatch;
		if ($prevMatch >= 0 && $nextMatch >= 0) {
			# Choose the one that extends the extisting syntenic block. If preceding hits are not
			# syntenic, pick one based on score and gap length
			if ( ! $prevHit->{'-first'} && $prevHit->{'-synt'}	== 1) {
				$bestMatch = $nextMatch;
			} elsif ( ! $prevHit->{'-first'} && $prevHit->{'-synt'} == -1) {
				$bestMatch = $prevMatch;
			} else {
				my $h1 = $locusHits->[$prevMatch];
				my $h2 = $locusHits->[$nextMatch];
				if ($h1->{'-score'} > $h2->{'-score'}) {
					$bestMatch = $prevMatch;
				} elsif ($h1->{'-score'} < $h2->{'-score'}) {
					$bestMatch = $nextMatch;
				} elsif ($h1->{'-gapLen'} < $h2->{'-gapLen'}) {
					$bestMatch = $prevMatch;
				} elsif ($h2->{'-gapLen'} > $h2->{'-gapLen'}) {
					$bestMatch = $nextMatch;
				} else {
					# Woah!
					$bestMatch = rand() < 0.5 ? $prevMatch : $nextMatch;
				}
			}
		} elsif ($prevMatch >= 0) {
			$bestMatch = $prevMatch;
		} elsif ($nextMatch >= 0) {
			$bestMatch = $nextMatch;
		} else {
			# None of the hits is syntenic. Pick one based on score and gap length. They should be sorted.
			$bestMatch = 0;
		}

		$locus->{'-best'} = $bestMatch;
		my $bestHit = $locus->{'-hits'}[$bestMatch];
		my $bestGene = $srcGenes->{$bestHit->{'-gene'}};
		if ($prevGene->{'-next'} eq $bestHit->{'-gene'}) {
			$bestHit->{'-synt'} = 1;
			$locus->{'-geneN'} = 0;
		} elsif ($prevGene->{'-prev'} eq $bestHit->{'-gene'}) {
			$bestHit->{'-synt'} = -1;
			$locus->{'-geneN'} = 0;
			# If the previous hit is the first on the scaffold, correct the sign for consistency
			if ($prevLocus->{'-first'}) {
				$prevLocus->{'-synt'} = -1;
				$prevHit->{'-synt'} = -1;
				$prevGene->{'-synt'} = -1;
			}
		} else {
			$bestHit->{'-synt'} = 0;
		}
		$locus->{'-synt'} = $bestHit->{'-synt'};
		$bestGene->{'-synt'} = $bestHit->{'-synt'} if ( ! $bestGene->{'-synt'} );

		# If genes are not syntenic but on the same scaffold, count the number of genes in between
		if ( ! $bestHit->{'-synt'} && $prevGene->{'-seq'} eq $bestGene->{'-seq'} ) {
			$locus->{'-geneN'} = 0;
			# Previous gene is upstream, move downstream (next)
			if ($prevGene->{'-beg'} < $bestGene->{'-beg'}) {
				while ($prevGene->{'-next'} ne $bestGene->{'-gene'}) {
					$locus->{'-geneN'}++;
					$prevGene = $srcGenes->{$prevGene->{'-next'}};
				}
			# Previous gene is downstream, move upstream (prev)
			} elsif ($prevGene->{'-beg'} > $bestGene->{'-beg'}) {
				while ($prevGene->{'-prev'} ne $bestGene->{'-gene'}) {
					$locus->{'-geneN'}--;
					$prevGene = $srcGenes->{$prevGene->{'-prev'}};
				}
			}
		} else {
			$locus->{'-geneN'} = 0;
		}

	}
}
		
	

# This is needed in case we have to break ties
srand($randSeed);

INFO("Read annotation from $pathGff");
my $srcGenes = readAnnotation($pathGff);
#my $srcGenes;
INFO("Link annotated genes");
linkAnnGenes($srcGenes);

#print join(", ", $srcGenes->{'311229'}{'-next'}, $srcGenes->{'311229'}{'-prev'}), "\n";
# 918732, 918729

INFO("Read BLAT results from $pathBlat");
my $loci = readBlatResults($pathBlat, $minCov, $mismatchPenalty, $gapPenalty);
$loci = compressHits($loci, $minOverlap);
#$Data::Dumper::Sortkeys = 1;
#print Dumper($loci);
#die("Noah!");

# The first iteration will not change the number of loci because none of the genes are initially
# marked as syntenic
my $cnt = 0;
my $diff1 = 0;
my $diff2 = 0;
do {
	processLoci($loci, $srcGenes);
	# Remove all secondary hits from genes that we know are syntenic
	$diff1 = removeSyntenic($loci, $srcGenes);
	# Decompress hits
	$diff2 = decompressHits($loci, $minOverlap);
	$cnt++;
	INFO("Iteration $cnt: $diff1 $diff2 " . scalar(@$loci)) if $cnt % 10 == 0;
	#if ($srcGenes->{914276}{'-synt'}) {
	#	die("They do exist! After $cnt");
	#}
} while ($diff1 != 0 || $diff2 != 0);
INFO("Converged after $cnt iterations");



INFO("Writing results");
open(FOUT, ">", $pathOut) or die("Unable to write to the output file ($pathOut). $!");
my @cols = qw/ HitScaf HitBeg HitEnd Gene GeneScaf GeneBeg GeneEnd IsFirst IsSynt GeneN HitN /;
print FOUT join("\t", @cols), "\n";

for my $locus (@$loci) {
	my $bestIdx = $locus->{'-best'};
	my $bestHit = $locus->{'-hits'}[$bestIdx];
	my $bestGene = $srcGenes->{$bestHit->{'-gene'}};
	my @fieldsOut = (
			@$bestHit{ qw/ -scaf -beg -end -gene / },
			@$bestGene{ qw/ -seq -beg -end / },
			@$locus{ qw/ -first -synt -geneN / },
			scalar(@{$locus->{'-hits'}}),
		);
	print FOUT join("\t", @fieldsOut), "\n";
}
close(FOUT);

INFO("Done");


__END__

=head1 NAME

03_mergeGeneLists.pl - merges the hit list from the BLAT output with the ordered gene list from the
annotation file.

=head1 SYNOPSIS

03_mergeGeneLists.pl -b blatHits.psl -g annotation.gff -o GeneList.txt

=head1 OPTIONS

=over 8

=item B<-b>

BLAT output file in the standard PSL format

=item B<-g>

Annotation file in GFF format

=item B<-o>

Tab-delimited output file containing the following columns: target scaffold, target start, target
end, query gene name, query gene sequence, query gene start, query gene end, synteny flag, number of
query genes between this hit and the previous hit, number of hits within the current locus. 

=item B<-c>

(Optional) Minimum query coverage for hit filtering. Default: 0.9

=item B<-m>

(Optional) Mismatch penalty for hit score calculation. Default: 2

=item B<-p>

(Optional) Gap penalty for hit score calculation. Default: 3

=item B<-s>

(Optional) Minimum overlap in bp between two hits required to combine them into a single locus.
Default: 100 

=back

=head1 DESCRIPTION

The script merges the hit list from the BLAT output with the ordered gene list from the annotation
file. The output will contain the synteny boolean flag indicating whether the current hit and the
previous hit are in synteny (on the same source scaffold without any other genes in-between). In
addition, it will list the number of genes present in the source between the genes corresponding to
this hit and the previous hit. That number will be 0 when the hits are syntenic or if the
corresponding genes are on two different scaffolds.

The script starts by reading the BLAT hits file and discarding the hits that have insufficient
source sequence coverage. Then the overlapping reads are compressed, i.e. they are combined into an
individual loci, so that they could be processed at the same time. The script check each pair of
neighboring hits to see if they correspond to syntenic genes. A hit is marked as syntenic if it has
the same orientation relative to the hit from the previous gene or if it is located at the beginning
of a scaffold. After all pairs are processed, the secondary hits of genes with syntenic hits are
removed from consideration. Loci left without hits are purged as well. Since that may affect the
overlap of compressed hits, each locus is checked and decompressed if necessary. It ensures that
each locus contains only overlapping hits. The script continues iteratively until the removal of
secondary hits and locus decompression fails to yield any changes in the locus number.

Why do we need to remove secondary hits from syntenic genes? Some of those hits have higher score
than the hits from the true positives. Thus, we have a better chance of selecting the true positive
from the remaining hits.

Why do we need to decompress loci? Occasionally, secondary hits may have very large gap between
matching regions and that gap may span several gene models. Once that secondary hit is removed, the
remaining hits would no longer overlap. This could be seen in the report where the locus before
non-syntenic locus had approximately the same HitN as the current locus' GeneN.

TODO: The script grew so complex that it would benefit from refactoring into a module with a test
suite.

=head1 AUTHOR

Roman Briskine, Universität Zürich

=cut


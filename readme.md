# De novo assembly of _Arabidopsis halleri_ ssp. _gemmifera_ (Tada mine, W302)

This file describes the pipeline used to generate _de novo_ assembly and gene annotation of _Arabidopsis halleri_ ssp. _gemmifera_ (W302, Tada mine) and accompanies the following paper:

Briskine RV, Paape T, Shimizu-Inatsugi R, Nishiyama T, Akama S, Sese J, Shimizu KK. Genome assembly and annotation of Arabidopsis halleri, a model for heavy metal hyperaccumulation and evolutionary ecology. Mol Ecol Resour. 2016. [DOI: 10.1111/1755-0998.12604](http://dx.doi.org/10.1111/1755-0998.12604)

The assembly is available from ENA under accession number [GCA_900078215.1](https://www.ebi.ac.uk/ena/data/view/GCA_900078215.1)

## 0. Environment and input data

### Environment

The initial assembly with ALLPATHS-LG was performed on a Linux cluster using 20 cores and 128 GiB of memory. All other computations were run on a Debian server (v7 and later v8) with 32 cores and 1 TiB of memory. The server uses SunGridEngine for job management and LMOD for software management. Each step of the pipeline indicates what additional applications and versions are used by providing module loading commands. All scripts reside in the local `src` directory. Logs are generally sent to the local `log` directory. Gaps in step numbering are caused by omission of the analyses that were not reported in the paper.

Unless otherwise stated or inferred, all commands are executed from the working directory using relative paths. Some applications fail to work properly with relative paths. In those cases, we use `$wd` variable to indicate the absolute path to the working directory. For instance, the absolute path to the input data is `$wd/00_input`.

All modules export a variable whose name matches the application name and points to the application prefix, e.g. `seq/picard` module exports `$picard` variable that points to picard installation directory. In addition some modules may export additional variables that point to `lib` and `include` directories within the installation prefix. Those variables have suffixes `_lib` and `_inc` respectively.

Some applications (e.g. `tophat2`) require `python2`. It will be provided via `pyenv`.


### Short reads

Short reads have been deposited to the DNA Data Bank of Japan (DDBJ) and received the following accession numbers: 

* DRX012199 – DRX012201 (paired-end reads)
* DRX045069 – DRX045074 (mate-pair reads)

The files have been placed into the `00_input` directory using the following name patterns.

* `Ahal_{200,500,800}-R{1,2}.fastq.gz` for paired-end reads
* `Ahal_{03,05,07,11,15,22}kb-R{1,2}.fastq.gz` for mate-pair reads


### _Arabidopsis lyrata_ genomic resources

_Arabidopsis lyrata_ v1.0 genomic sequences, gene annotations, and coding sequences were downloaded from the phytozome website on 2012-12-13.

* `Alyr_jgi.fa` := assembly
* `Alyr_jgi.gff.gz` := gene annotation
* `Alyr_jgi_cds.fa.gz` := coding sequences


### _Arabidopsis thaliana_ genomic resources

TAIR10 genomic sequences and gene annotations were downloaded from the [TAIR FTP site](ftp://ftp.arabidopsis.org/home/tair/Genes/TAIR10_genome_release). Chromosome display names were updated to use shorter format. CDS were extracted from the genome based on the gene annotation.

```shell
curl -L $tairFtp/TAIR10_gff3/TAIR10_GFF3_genes.gff | xz - > 00_input/Atha.gff.xz
curl -L $tairFtp/TAIR10_chromosome_files/TAIR10_chr_all.fas | xz - > 00_input/Atha_chr_all.fas.xz
xzcat 00_input/Atha_chr_all.fas.xz | sed -r 's/^>(.).+/>Chr\U\1/' | xz - > Atha.fa.xz
src/extractCds.pl -f 00_input/Atha.fa.xz -g 00_input/Atha.gff.xz -l -o 00_input/Atha_cds.fa.xz
```


### _Arabidopsis halleri_ RNA-seq data

See the main text about the origin of the RNA-seq data.

* Leaf sample := `00_input/RNA_leaf-R{1,2}.fastq.gz`
* Root sample := `00_input/RNA_root-R{1,2}.fastq.gz`


### HMA4 and MTP1 BACs

We downloaded HMA4 (17L07 and 7C17) and MTP1 BACs of _A. halleri_ from NCBI nucmer database on 2015-02-19. 

* gi|291197507|emb|FN428855.1| saved as MTP1_locus.fa
* gi|184160084|gb|EU382072.1| saved as HMA4_17L07.fa
* gi|184160092|gb|EU382073.1| saved as HMA4_7C17.fa

The HMA4 BACs were spliced with minimus2 from Amos v3.1.0 package.

```shell
module load bio/amos/3.1.0
cd 00_input
mkdir tmp
cat HMA4_*.fa > tmp/both.seq
cd tmp
toAmos -s both.seq -o both.afg
minimus2 both
sed -r 's/^>.+$/>HMA4_locus/' both.fasta > ../HMA4_locus.fasta
cd ..
```

Replace display id with a short one for MTP1.

```shell
sed -r -i.genbank 's/^>.+$/>MTP1_locus/' MTP1_locus.fa
```



### Previous version of the assembly

Previous version of the assembly (v1.0) is available from DDBJ (PRJDB1392).

Assembly stats for v1.0 using the expected genome size of 240 Mb. The second command ignores scaffolds that are shorter than 932 bp, which is the shortest scaffold length in v2.0. This would allow direct comparison between the assemblies. (Even though the reporting length threshold for ALLPATHS-LG is 1000 bp, it reported several shorter scaffolds.)

```shell
src/00_calcAssemblyStats.pl -i 00_input/Ahal_v1.0.fa -e 240 > 00_input/Ahal_v1.0.stats
src/00_calcAssemblyStats.pl -i 00_input/Ahal_v1.0.fa -e 240 -m 932 > 00_input/Ahal_v1.0_m932.stats
```




## 1. _De novo_ assembly

First run using the expected insert sizes from `src/01_libs_1st.csv`

```shell
mkdir -p 01_denovo/1st/cache
module load seq/allpathslg/50599 seq/picard/1.115
CacheLibs.pl CACHE_DIR=$wd/01_denovo/1st/cache IN_LIBS_CSV=src/01_libs_1st.csv ACTION=Add
CacheGroups.pl CACHE_DIR=$wd/01_denovo/1st/cache PICARD_TOOLS_DIR=$picard IN_GROUPS_CSV=src/01_groups.csv TMP_DIR=$wd/tmp ACTION=Add HOSTS=20
CacheToAllPathsInputs.pl CACHE_DIR=$wd/01_denovo/1st/cache GROUPS="{PE{200,500,800},MP{03,05,07,11,15,22}}" DATA_DIR=$wd/01_denovo/1st/data PLOIDY=2
RunAllPathsLG PRE=$wd/01_denovo DATA_SUBDIR=data RUN=default SUBDIR=default REFERENCE_NAME=1st THREADS=20
```


Second run uses the insert sizes reported by ALLPATHS-LG in the first run. These were placed in `src/02_libs_2nd.csv`.

```shell
CacheLibs.pl CACHE_DIR=$wd/01_denovo/2nd/cache IN_LIBS_CSV=src/01_libs_2nd.csv ACTION=Add
CacheGroups.pl CACHE_DIR=$wd/01_denovo/2nd/cache PICARD_TOOLS_DIR=$picard IN_GROUPS_CSV=src/01_groups.csv TMP_DIR=$wd/tmp ACTION=Add HOSTS=20
CacheToAllPathsInputs.pl CACHE_DIR=$wd/01_denovo/2nd/cache GROUPS="{PE{200,500,800},MP{03,05,07,11,15,22}}" DATA_DIR=$wd/01_denovo/2nd/data PLOIDY=2
RunAllPathsLG PRE=$wd/01_denovo DATA_SUBDIR=data RUN=default SUBDIR=default REFERENCE_NAME=2nd THREADS=20
```


For historical reasons and to make subsequent commands shorter, we symlink the final assembly to a file in the `00_input` directory. Then, we calculate statistics for the v2.0 assembly.

```shell
ln -s $wd/01_denovo/2nd/default/ASSEMBLIES/default/final.assembly.fasta 00_input/Ahal_v2.0.fa
src/00_calcAssemblyStats.pl -i 00_input/Ahal_v2.0.fa -e 240 > 00_input/Ahal_v2.0.stats
```




## 2. _Arabidopsis lyrata_ homologues

Identify the locations of _A. lyrata_ homologues in _A. halleri_ v2.0 assembly by running BLAT on coding sequences of _A. lyrata_ against the assembly sequences. Since we expect relatively high divergence between _A. lyrata_ and _A. halleri_, we will keep the minimum identity at 90 percent. By default, the maximum intron size is 750 kb but that is too large for plant genomes. In _A. lyrata_, the longest intron is 44,703 bp. This is most likely a misassembly or a pseudogene. Homologues in other species have much smaller introns. However, there are several other introns longer than 30 kb. Therefore, a reasonable value for that parameter would be 50 kb.

```shell
mkdir 02_blat
module load bio/blat/36
src/02_findMaxIntronSize.pl 00_input/Alyr_jgi.gff.gz 10
blat 00_input/Ahal_v2.0.fa 00_input/Alyr_jgi_cds_po.fa.gz -maxIntron=50000 02_blat/hitsAll.psl
```


Select best hit for each gene with minimum query coverage of 85%.

```shell
src/02_selectBest.pl -i 02_blat/hitsAll.psl -o 02_blat/hitsBest.psl -c 0.85
```




## 3. Synteny information

Merge the information about gene order from the _A. lyrata_ gene annotation file and the filtered BLAT output.

```shell
mkdir 03_merge
src/03_mergeGeneLists.pl -b 02_blat/hitsAll.psl -g 00_input/Alyr_jgi.gff.gz -o 03_merge/synteny.txt
```




## 4. Primary and secondary syntenic regions

Primary syntenic regions are those that are not inserted into larger syntenic regions. A primary region may be very short if it is located at the beginning or the end of a scaffold. It can also be short if it lies between two long primary syntenic regions. Secondary syntenic regions are located within a primary syntenic region but are syntenic to another scaffold. Secondary regions are shorter than 100 kb by definition. In other words, primary region is extended across a non-syntenic region as long as the non-syntenic region is shorter than 100 kb and the distance between the corresponding syntenic genes on the reference is less than 100 kb.


```shell
mkdir 04_regions
src/04_compressHits.pl -i 03_merge/synteny.txt -p 04_regions/primary.txt -s 04_regions/secondary.txt
```


In total, there are 1303 primary and 1874 secondary syntenic regions. The number of primary regions is smaller than the total number of scaffolds because some scaffolds do not have any syntenic hits.



## 5. Cutting sites

We can only cut where two regions are separated by a long stretch of Ns. Since not all primary regions are separated by Ns, we need to find all sites where we can cut. We chose the threshold of 50 for the minimum number of Ns. Secondary regions need to be cut out and the sites filled with Ns with length corresponding to the distance between the flanking syntenic genes. In some cases, primary site overlaps with secondary site. Those cases are not counted. They are completely ignored.

```shell
mkdir 05_cutSites
src/05_findCutSites.pl -p 04_regions/primary.txt -a 00_input/Ahal_2.0.fa -o 05_cutSites/cutSites.txt
src/05_findSliceSites.pl -s 04_regions/secondary.txt -a 00_input/Ahal_2.0.fa -o 05_cutSites/cutSitesSecondary.txt
```


Cuts can be made at 454 out of 725 primary cutting sites. Out of 1328 secondary sites, 383 can be cut out.



## 6. Cut the assembly at primary sites

The output assembly is assigned version `2.1`.

```shell
mkdir 06_split
src/06_cutAssembly.pl -a 00_input/Ahal_v2.0.fa -s 05_cutSites/cutSites.txt -o 06_split/Ahal.fa -m 06_split/AhalScafMap.txt
src/00_calcAssemblyStats.pl -i 06_split/Ahal.fa -e 240 > 06_split/Ahal.stats
```




## 7. Omitted



## 8. Omitted



## 9. Merge back _HMA4_ region

According to the previously published BAC sequences (EU382072.1 and EU382073.1), _A. halleri_ contains _HMA4_ tandem duplications that are absent from _A. lyrata_. Before the synteny-based cutting, _HMA4_ locus resided on a single scaffold (scaffold_50) but after the cutting the region appeared on 4 scaffolds (scaffold_174, scaffold_1402, scaffold_329, and scaffold_392). It is possible that the list of cutting sites contains other false positives but further validation is not possible because BACs are only available for _HMA4_ and _MTP1_ regions. The latter remained on a single scaffold after cutting and need not to be updated.

Rather than creating a new script to merge the scaffolds and renumbering them, we will remove the cut points between new scaffolds 174 and 392 specified in the `05_cutSites/cutSites.txt` (also, see `06_split/AhalScafMap.txt`). Then we will run the existing `06_cutAssembly.pl` script.

The output assembly is assigned version `2.2`.

```shell
mkdir 09_hma4Fix
sed -rn -e '1,/^scaffold_50\t512197/ p' -e '/^scaffold_50\t1002313/,$ p' 05_cutSites/cutSites.txt > 09_hma4Fix/cutSites.txt
src/06_cutAssembly.pl -a 00_input/Ahal_v2.0.fa -s 09_hma4Fix/cutSites.txt -o 09_hma4Fix/Ahal.fa -m 09_hma4Fix/AhalScafMap.txt
ln -s $wd/09_hma4Fix/Ahal.fa 00_input/Ahal_v2.2.fa
```




## 10. Transposable elements

TE locations will be used as hints for AUGUSTUS when annotating the genome. We chose to run only RepeatMasker because Censor was too sensitive producing a large number of very small fragments.

RepeatMasker was run on 4 processors (`-pa 4`). We ignore low complexity repeats with `-nolow`, limit the search to green plants `-species viridiplantae`, and use NCBI engine `-e ncbi`.

```shell
mkdir -p 10_te/RepeatMasker
module load bio/RepeatMasker/4.0.5
nice RepeatMasker -pa 4 -nolow -species viridiplantae -dir 10_te/RepeatMasker -e ncbi 09_hma4Fix/Ahal.fa >& log/10_rm &
```


The output does not comply with GFF v3 and needs to be converted.

```shell
src/10_makeGffRm.pl -i 10_te/RepeatMasker/Ahal.fa.out -o 10_te/RepeatMasker/RmTe.gff
```




## 11. Omitted



## 12. Annotate the genome

Annotate genome v2.2 with AUGUSTUS. Here, we did a preliminary run without any hints (NH - No Hints) and a separate run with RepeatMasker hints (NRS - No RNA-Seq). RNA-seq data was not used at this point.

Generate hints.

```shell
mkdir 12_annotation
src/12_makeTeHints.pl -i 10_te/RepeatMasker/RmTe.gff > 12_annotation/RmHints.gff
```


We use the default extrinsic parameters except the RepeatMasker nonexonpart penalty which was increased from `1.01` to `1.15`.

```shell
module load seq/augustus/3.0.3
mkdir -p 12_annotation/{NH,NRS}
nice augustus --species=arabidopsis 09_hma4Fix/Ahal.fa --outfile=12_annotation/NH/augustus.gtf --gff3=on > log/12_augustusNH &
nice augustus --species=arabidopsis 09_hma4Fix/Ahal.fa --outfile=12_annotation/NRS/augustus.gtf --hintsfile=12_annotation/RmHints.gff --extrinsicCfgFile=src/12_extrinsic.M.RM.E.W.cfg --UTR=on --gff3=on > log/12_augustusNRS &
```


Provide unique IDs for CDS, rename transcript to mRNA (as in TAIR), remove comments, pad gene ids with zeros for easier sorting.

```shell
src/18_fixAugustusGff.pl -i 12_annotation/NRS/augustus.gtf -m -p 5 | xz - > 12_annotation/NRS/annotation.gff.xz
src/18_fixAugustusGff.pl -i 12_annotation/NH/augustus.gtf  -m -p 5 | xz - > 12_annotation/NH/annotation.gff.xz
```


Annotation stats

```shell
src/AnnotStats.pl -a 12_annotation/NRS/annotation.gff.xz -f textile -v d -u 12_annotation/NRS/utrLengths.txt
src/AnnotStats.pl -a 12_annotation/NH/annotation.gff.xz  -f textile -v d -u 12_annotation/NH/utrLengths.txt
```


Extract CDS. The first command retrieves only the longest transcripts per gene while the second retrieves all transcripts. In this case, both commands should produce the same results as the annotations have only one transcript per gene.

```shell
# NH
src/extractCds.pl -f 09_hma4Fix/Ahal.fa -g 12_annotation/NH/annotation.gff.xz -o 12_annotation/NH/cds.fa.xz -l
src/extractCds.pl -f 09_hma4Fix/Ahal.fa -g 12_annotation/NH/annotation.gff.xz -o 12_annotation/NH/cds_all.fa.xz
# NRS
src/extractCds.pl -f 09_hma4Fix/Ahal.fa -g 12_annotation/NRS/annotation.gff.xz -o 12_annotation/NRS/cds.fa.xz -l
src/extractCds.pl -f 09_hma4Fix/Ahal.fa -g 12_annotation/NRS/annotation.gff.xz -o 12_annotation/NRS/cds_all.fa.xz
```


Identify reciprocal best blast hits (RBH) using the script from Akama et al., 2014.

```shell
module load bio/blast/2.2.29 dev/ruby/2.2.3
rbh=12_annotation/rbh
mkdir -p $wd
unxz -c 12_annotation/NH/cds.fa.xz > $rbh/AhalNH.fa
unxz -c 12_annotation/NRS/cds.fa.xz > $rbh/AhalNRS.fa
gzip -dc 00_input/Alyr_jgi_cds.fa.gz > $rbh/Alyr.fa
unxz -c 00_input/Atha_cds.fa.xz > $rbh/Atha.fa

for i in AhalNH AhalNRS Alyr Atha; do
	for j in AhalNH AhalNRS Alyr Atha; do
		rbh4 -1 $rbh/${i}.fa -2 $rbh/${j}.fa -t 8 -r $rbh
		rbh4 -1 $rbh/${i}.fa -2 $rbh/${j}.fa -t 8 -r $rbh -o
		rbh4 -1 $rbh/${j}.fa -2 $rbh/${i}.fa -t 8 -r $rbh -o
	done
done
rm $rbh/*.fa*
```



## 13. Align RNA-seq data

Align RNA-seq data to the assembled genome. The output will be used as hints for AUGUSTUS. We will compare the output from STAR and TopHat. We align against the genome that has transposable elements masked with RepeatMasker. In principle, this should improve the quality of genic transcript alignments.

### Read suffixes

In order to find read pairs, the filterBam application included in AUGUSTUS uses suffixes rather than the bitwise flag. So, we have to append suffixes to all read names. Since TopHat clips the standard suffixes ('/1' and '/2'), we have to append non-standard ones ('-1' and '-2'). Meanwhile, STAR uses the name of the first read to name both reads in the output. Hence, it will be necessary to update suffixes in the STAR output again.

```shell
mkdir -p 13_align/{genome,root,leaf}
zcat 00_input/RNA_leaf-R1.fastq.gz | perl -ple 's/^(\S+)/$1-1/ if $. % 4 == 1;' | gzip - > 13_align/leaf/prefixed_R1.fastq.gz
zcat 00_input/RNA_leaf-R2.fastq.gz | perl -ple 's/^(\S+)/$1-2/ if $. % 4 == 1;' | gzip - > 13_align/leaf/prefixed_R2.fastq.gz
zcat 00_input/RNA_root-R1.fastq.gz | perl -ple 's/^(\S+)/$1-1/ if $. % 4 == 1;' | gzip - > 13_align/root/prefixed_R1.fastq.gz
zcat 00_input/RNA_root-R2.fastq.gz | perl -ple 's/^(\S+)/$1-2/ if $. % 4 == 1;' | gzip - > 13_align/root/prefixed_R2.fastq.gz
```



### STAR

Build the genomic index and align the reads. We use relatively strict mismatch parameters and limit the intron maximum length to 40 kb. All but one intron in _A. lyrata_ are shorter than 40 kb.

```shell
module load seq/star/2.4.0i
cd 13_align
STAR --runMode genomeGenerate --genomeDir genome --genomeFastaFiles ../10_te/RepeatMasker/Ahal.fa.masked --runThreadN 8
cd leaf
nice STAR --genomeDir ../genome --readFilesIn prefixed_R1.fastq.gz prefixed_R2.fastq.gz --readFilesCommand zcat --runThreadN 8 --outFilterMultimapNmax 5 --outFilterMismatchNmax 4 --outFilterMismatchNoverLmax 0.05 --alignIntronMax 40000 --alignMatesGapMax 1000 >& ../../log/13_star_leaf &
cd ../root
nice STAR --genomeDir ../genome --readFilesIn prefixed_R1.fastq.gz prefixed_R2.fastq.gz --readFilesCommand zcat --runThreadN 8 --outFilterMultimapNmax 5 --outFilterMismatchNmax 4 --outFilterMismatchNoverLmax 0.05 --alignIntronMax 40000 --alignMatesGapMax 1000 >& ../../log/13_star_root &
```


Sort the reads by coordinate

```shell
module load seq/samtools/1.2
cd 13_align/leaf
samtools sort -@ 8 -m 32G -T tmpSort -o Aligned.bam Aligned.out.sam
cd ../root
samtools sort -@ 8 -m 32G -T tmpSort -o Aligned.bam Aligned.out.sam
```


STAR uses the name of the first read for both reads and relies on the bitwise flag to differentiate them (`flag & 64` for the first, and `flag & 128` for the second).

```shell
module load seq/samtools/1.2
for s in leaf root; do
	samtools view -h 13_align/$s/Aligned.bam | perl -nle 'if (/^@/) { print $_; } else { @F = split("\t", $_, 10); $sfx = ""; if ($F[1] & 64) { $sfx = "-1" } elsif ($F[1] & 128) { $sfx = "-2" }; $F[0] =~ s/(-[12])?$/$sfx/; print join("\t", @F); }' | samtools view -b - > 13_align/$s/prefixed_aligned.bam
done
```


Before producing hints, the bam files for root and leaf tissues need to be merged. That will naturally merge both intron and exonpart hints derived from either read group, thereby saving space and computation time. Although in this particular case, the read names are unique, it would be prudent to add read group names while merging. This will ensure that reads from different samples are still kept separately. Since samtools uses the file name to denote a read group, the input files have to be symlinked.

```shell
module load seq/samtools/1.2
ln -s 13_align/leaf/prefixed_aligned.bam 13_align/leaf.bam
ln -s 13_align/root/prefixed_aligned.bam 13_align/root.bam
echo -e '@RG\tID:leaf\tPL:Illumina\tPU:leaf\tLB:leaf\tSM:leaf' >  13_align/readGroups.txt
echo -e '@RG\tID:root\tPL:Illumina\tPU:root\tLB:root\tSM:root' >> 13_align/readGroups.txt
samtools merge -rh 13_align/readGroups.txt 13_align/merged.bam 13_align/leaf.bam 13_align/root.bam
```


Extract hints. By default, `wig2hints.pl` uses the radius of 0. This causes exonparts that are one bp long. Also, due to the default width of 40, consecutive hits are separated by at least 40 bp. To get better hints, we will use the radius of 4.5 and the width of 10.

```shell
module load seq/augustus/3.0.3
bam2hints --intronsonly --in=13_align/merged.bam --out 13_align/hints.intron.gff
bam2wig 13_align/merged.bam > 13_align/cov.wig
wig2hints.pl --width=10 --margin=10 --minthresh=2 --minscore=4 --prune=0.1 --type=ep --src=W --radius=4.5 --pri=4 < 13_align/cov.wig > 13_align/hints.ep.gff
rm 13_align/cov.wig
```



### TopHat2 and Bowtie2

Create directories and symbolic links.

```shell
mkdir 13_align/{genome,leaf,root}_bowtie
ln -s ../../10_te/RepeatMasker/Ahal.fa.masked 13_align/genome_bowtie/Ahal.fa
```


Create the index

```shell
module load seq/bowtie2/2.2.4
bowtie2-build 13_align/genome_bowtie/Ahal.fa 13_align/genome_bowtie/Ahal
```


Align with bowtie2 v2.2.4 and tophat v2.0.13. Hint: in case of a problem check the logs in the output directory.

```shell
module load seq/tophat/2.0.13 seq/bowtie2/2.2.4
pyenv shell 2.7.8
tophat2 -p 4 -r 200 --max-intron-length 40000 -o 13_align/leaf_bowtie --library-type fr-unstranded 13_align/genome_bowtie/Ahal 13_align/leaf/prefixed_R1.fastq.gz 13_align/leaf/prefixed_R2.fastq.gz
tophat2 -p 4 -r 200 --max-intron-length 40000 -o 13_align/root_bowtie --library-type fr-unstranded 13_align/genome_bowtie/Ahal 13_align/root/prefixed_R1.fastq.gz 13_align/root/prefixed_R2.fastq.gz
```




## 14. Train AUGUSTUS

Create hints from the RNA-seq alignments and RepeatMasker data, then use them for the first annotation run of AUGUSTUS. Since STAR aligned more reads than TopHat, we will use intron and exonpart hints from STAR alignments. 

Overall, we have 4,005,369 hints.

  Hints | File 
-------:|:-----
  68173 | 12_annotation/RmHints.gff 
 215702 | 13_align/hints.intron.gff 
3721494 | 13_align/hints.ep.gff     

Initialise, adjust the extrinsic parameters and combine hints. We will use the values specified in the pipeline recommendations except for higher RepeatMasker nonexonpart penalty (`1.15` rather than `1.01`).

```shell
mkdir 14_augPrelim
cat 12_annotation/RmHints.gff 13_align/hints.intron.gff 13_align/hints.ep.gff > 14_augPrelim/hints.gff
```


Run AUGUSTUS. Note that we are running without gff3 flag because the output may be parsed in a special way that requires the default formatting.

```shell
module load seq/augustus/3.0.3
augustus --species=arabidopsis --extrinsicCfgFile=src/14_extrinsic.M.RM.E.W.cfg --alternatives-from-evidence=true --hintsfile=14_augPrelim/hints.gff --allow_hinted_splicesites=atac --introns=on --genemodel=complete --UTR=on 09_hma4Fix/Ahal.fa > 14_augPrelim/output.gff
```




## 15. Align to exon-exon junctions

Since the reads should align without gaps to exon junctions, we will use bowtie2 rather than STAR. STAR seems to be very aggressive while looking for gapped alignments. The output will be filtered to discard unaligned reads. By default, `intron2exex.pl` uses 75 bp of the flanking sequence. Since our RNA-seq libraries have 101-bp reads, we need to increase the length by using the `--flank` parameter.

Extract junctions and build the database.

```shell
module load seq/augustus/3.0.3
mkdir 15_exonJunc
grep -hP "\tintron\t" 14_augPrelim/output.gff 14_augPrelim/hints.gff | perl -nale 'print "$F[0]:$F[3]-$F[4]";' | sort -u > 15_exonJunc/introns.txt
intron2exex.pl --introns=15_exonJunc/introns.txt --flank=101 --seq=10_te/RepeatMasker/Ahal.fa.masked --exex=15_exonJunc/exex.fa --map=15_exonJunc/map.psl
```


Align the reads using bowtie2

```shell
module load seq/bowtie2/2.2.4 seq/samtools/1.2
bowtie2-build 15_exonJunc/exex.fa 15_exonJunc/exex
bowtie2 -x 15_exonJunc/exex -U 13_align/leaf/prefixed_R1.fastq.gz -p 4 | samtools view -F 4 - > 15_exonJunc/bowtie2_leaf_R1.sam
bowtie2 -x 15_exonJunc/exex -U 13_align/leaf/prefixed_R2.fastq.gz -p 4 | samtools view -F 4 - > 15_exonJunc/bowtie2_leaf_R2.sam
bowtie2 -x 15_exonJunc/exex -U 13_align/root/prefixed_R1.fastq.gz -p 4 | samtools view -F 4 - > 15_exonJunc/bowtie2_root_R1.sam
bowtie2 -x 15_exonJunc/exex -U 13_align/root/prefixed_R2.fastq.gz -p 4 | samtools view -F 4 - > 15_exonJunc/bowtie2_root_R2.sam
```


Map local exon junction alignments to the genome coordinates. The last argument is the flanking region size. It should be set if a non-default value was used with `intron2exex.pl`.

```shell
module load seq/augustus/3.0.3 seq/samtools/1.2
samtools view -H 13_align/leaf/prefixed_aligned.bam > 15_exonJunc/leaf.sam
samMap.pl 15_exonJunc/bowtie2_leaf_R1.sam 15_exonJunc/map.psl 101 >> 15_exonJunc/leaf.sam
samMap.pl 15_exonJunc/bowtie2_leaf_R2.sam 15_exonJunc/map.psl 101 >> 15_exonJunc/leaf.sam
samtools view -H 13_align/root/prefixed_aligned.bam > 15_exonJunc/root.sam
samMap.pl 15_exonJunc/bowtie2_root_R1.sam 15_exonJunc/map.psl 101 >> 15_exonJunc/root.sam
samMap.pl 15_exonJunc/bowtie2_root_R2.sam 15_exonJunc/map.psl 101 >> 15_exonJunc/root.sam
```


Convert to bam

```shell
module load seq/samtools/1.2
samtools view -b -o 15_exonJunc/leaf.bam 15_exonJunc/leaf.sam
samtools view -b -o 15_exonJunc/root.bam 15_exonJunc/root.sam
rm 15_exonJunc/*.sam
```




## 16. Combine alignment data

Combine the data from the original alignment to the genome and the exon-junction alignment. Reads form the genome alignment will be filtered to exclude spliced reads (CIGAR does not contain Ns).

Filter out spliced alignments.

```shell
module load seq/bamtools/2.3.0
mkdir 16_combineAln
bamtools filter -in 13_align/leaf/prefixed_aligned.bam -out 16_combineAln/leaf_unspliced.bam -script $AUGUSTUS_CONFIG_PATH/../auxprogs/auxBamFilters/operation_N_filter.txt
bamtools filter -in 13_align/root/prefixed_aligned.bam -out 16_combineAln/root_unspliced.bam -script $AUGUSTUS_CONFIG_PATH/../auxprogs/auxBamFilters/operation_N_filter.txt
```


Combine spliced (step 15) and unspliced (this step) reads.

```shell
module load seq/bamtools/2.3.0
bamtools merge -in 15_exonJunc/leaf.bam -in 16_combineAln/leaf_unspliced.bam -out 16_combineAln/leaf_merged.bam
bamtools merge -in 15_exonJunc/root.bam -in 16_combineAln/root_unspliced.bam -out 16_combineAln/root_merged.bam
```


Sort and filter unpaired reads.

```shell
module load seq/samtools/1.2 seq/augustus/3.0.3 seq/bamtools/2.3.0
samtools sort -@ 8 -n -m 32G -T tmpSortLeaf -o 16_combineAln/leaf_merged_sortn.bam 16_combineAln/leaf_merged.bam 
samtools sort -@ 8 -n -m 32G -T tmpSortRoot -o 16_combineAln/root_merged_sortn.bam 16_combineAln/root_merged.bam 

filterBam --uniq --paired --in 16_combineAln/leaf_merged_sortn.bam --out 16_combineAln/leaf_merged_filtered.bam
filterBam --uniq --paired --in 16_combineAln/root_merged_sortn.bam --out 16_combineAln/root_merged_filtered.bam

bamtools merge -in 16_combineAln/leaf_merged_filtered.bam -in 16_combineAln/root_merged_filtered.bam -out 16_combineAln/combined.bam

samtools sort -@ 8 -m 64G -T tmpSortComb -o 16_combineAln/combined_sorted.bam 16_combineAln/combined.bam
```

Create hints

```shell
module load seq/augustus/3.0.3
bam2hints --intronsonly --in=16_combineAln/combined_sorted.bam --out 16_combineAln/combined.intron.gff
bam2wig 16_combineAln/combined_sorted.bam > 16_combineAln/combined.cov.wig
wig2hints.pl --width=10 --margin=10 --minthresh=2 --minscore=4 --prune=0.1 --type=ep --src=W --radius=4.5 --pri=4 < 16_combineAln/combined.cov.wig > 16_combineAln/combined.ep.gff
```



## 17. Run AUGUSTUS

Run AUGUSTUS using the data compiled using the first run. We will use the same extrinsic parameters as for the first run.

Combine RepeatMasker hints, intron hints, and exonpart hints.

```shell
mkdir 17_augFinal
cat 12_annotation/RmHints.gff 16_combineAln/combined.intron.gff 16_combineAln/combined.ep.gff > 17_augFinal/hints.gff
```


Compared to the first run, we no longer require complete gene models and the reporting of the introns as separate entries. We still want to keep the UTR prediction, because we provide exonpart hints from RNA-seq. Also, we want the output in GFF3 format because this is the final output.

```shell
module load seq/augustus/3.0.3
augustus --species=arabidopsis --extrinsicCfgFile=src/17_extrinsic.M.RM.E.W.cfg --alternatives-from-evidence=true --hintsfile=17_augFinal/hints.gff --allow_hinted_splicesites=atac --UTR=on --gff3=on 09_hma4Fix/Ahal.fa > 17_augFinal/output.gff
```


Provide unique IDs for CDS, rename transcript to mRNA (as in TAIR), remove comments, pad gene ids with zeros for easier sorting.

```shell
src/18_fixAugustusGff.pl -i 17_augFinal/output.gff -m -p 5 | xz - > 17_augFinal/annotation.gff.xz
```


Annotation stats

```shell
src/AnnotStats.pl -a 17_augFinal/annotation.gff.xz -f textile -v d -o 17_augFinal/overlaps.txt -u 17_augFinal/utrLengths.txt
```


Comparison between Ahal v1.2, Ahal v2.2, JGI Alyr v1.0, TAIR 10, AhalNRS, and AhalNH. (NRS = annotation generated without RNA-seq hints, NH = annotation generated without any hints.)

```shell
mkdir 17_augFinal/AhalV1
cd 17_augFinal/AhalV1
sed -r 's/Name=scaffold[0-9]+\./Name=/' 00_input/Ahal_v1.0.gff | xz > 17_augFinal/AhalV1/output.gff.xz
src/18_fixAugustusGff.pl -i 17_augFinal/AhalV1/output.gff.xz -m -p 5 | xz > 17_augFinal/AhalV1/annotation.gff.xz
src/extractCds.pl -f 00_input/Ahal_v1.0.fa -g 17_augFinal/AhalV1/annotation.gff.xz -l --minLen 201 -o 17_augFinal/AhalV1/cds.fa.xz
```


Extract CDS.

```shell
src/extractCds.pl -f 09_hma4Fix/Ahal.fa -g 17_augFinal/annotation.gff.xz -o 17_augFinal/cds.fa.xz -l
src/extractCds.pl -f 09_hma4Fix/Ahal.fa -g 17_augFinal/annotation.gff.xz -o 17_augFinal/cds_all.fa.xz
```


Identify reciprocal best blast hits (RBH) using the script from Akama et al., 2014. AhalV1 contains only transcripts longer than 200 bp to match the results in Akama et al., 2014.

```shell
rbh=17_augFinal/rbh
mkdir -p $rbh
unxz -c 17_augFinal/cds.fa.xz > $rbh/Ahal.fa
unxz -c 17_augFinal/AhalV1/cds.fa.xz > $rbh/AhalV1.fa
unxz -c 12_annotation/NH/cds.fa.xz > $rbh/AhalNH.fa
unxz -c 12_annotation/NRS/cds.fa.xz > $rbh/AhalNRS.fa
gzip -dc 00_input/Alyr_jgi_cds.fa.gz > $rbh/Alyr.fa
unxz -c 00_input/Atha_cds.fa.xz > $rbh/Atha.fa

for i in Ahal AhalV1 AhalNH AhalNRS Alyr Atha; do
	for j in Ahal AhalV1 AhalNH AhalNRS Alyr Atha; do
		rbh4 -1 $rbh/${i}.fa -2 $rbh/${j}.fa -t 8 -r $rbh
		rbh4 -1 $rbh/${i}.fa -2 $rbh/${j}.fa -t 8 -r $rbh -o
		rbh4 -1 $rbh/${j}.fa -2 $rbh/${i}.fa -t 8 -r $rbh -o
	done
done
rm $rbh/*.fa*
```

Compare this annotation to AhalNRS and AhalNH

```shell
src/AnnotComp.pl -1 17_augFinal/annotation.gff.xz -2 12_annotation/NH/annotation.gff.xz -a Ahal -b AhalNH -f textile -r 17_augFinal/fusedAhalAhalNH.txt -s 17_augFinal/splitAhalAhalNH.txt -v d
src/AnnotComp.pl -1 17_augFinal/annotation.gff.xz -2 12_annotation/NRS/annotation.gff.xz -a Ahal -b AhalNRS -f textile -r 17_augFinal/fusedAhalAhalNRS.txt -s 17_augFinal/splitAhalAhalNRS.txt -v d
```




## 18. Annotate HMA4 and MTP1 BACs

The published HMA4 and MTP1 BACs have much fewer gene models than we have in our assembly. However, the present genes are mostly syntenic. There are a few exceptions where gene models between HMA4 copies are reversed in some instances. We re-annotated the published BACs with AUGUSTUS and compared the output with the gene annotation for the Tada mine isolate.

### 18.1 Mask repeats

Identify TEs and low complexity repeats for AUGUSTUS to use as hints by running RepeatMasker v4.0.5 with RepBase v19.04 (20140131).

```shell
mkdir -p 18_annotateBACs/{HMA4,MTP1}/01_RM
module load bio/RepeatMasker/4.0.5
RepeatMasker -pa 4 -nolow -species viridiplantae -dir 18_annotateBACs/HMA4/01_RM -e ncbi 00_input/HMA4_locus.fa >& log/18.01_RM_HMA4
RepeatMasker -pa 4 -nolow -species viridiplantae -dir 18_annotateBACs/MTP1/01_RM -e ncbi 00_input/MTP1_locus.fa >& log/18.01_RM_MTP1

src/10_makeGffRm.pl -i 18_annotateBACs/HMA4/01_RM/HMA4_locus.fa.out -o 18_annotateBACs/HMA4/01_RM/RmTe.gff
src/10_makeGffRm.pl -i 18_annotateBACs/MTP1/01_RM/MTP1_locus.fa.out -o 18_annotateBACs/MTP1/01_RM/RmTe.gff
```

### 18.2 Generate hints

AUGUSTUS splices the first MTP1 copy with the previous gene (the ortholog of AT2G46810). To prevent this, we have to add a manual hint for the MTP1 region that describes the exact CDS of the first MTP1 copy.

```shell
perl -le 'print join("\t", qw/MTP1_locus Manual CDS 37716 38885 . - . Name=MTP1-1;source=M/)' > 18_annotateBACs/MTP1/02_hints.gff
src/12_makeTeHints.pl -i 18_annotateBACs/HMA4/01_RM/RmTe.gff >  18_annotateBACs/HMA4/02_hints.gff
src/12_makeTeHints.pl -i 18_annotateBACs/MTP1/01_RM/RmTe.gff >> 18_annotateBACs/MTP1/02_hints.gff
```


We use the same extrinsic parameters as for the whole genome except we set CDS malus for manual hints to 1000. The large malus is necessary to make sure that the hint for the first MTP1 copy is observed. The change does not affect HMA4 because it does not have any manual hints.


### 18.3 Run Augustus

```shell
modue load seq/augustus/3.0.3
augustus --species=arabidopsis 00_input/HMA4_locus.fa --outfile=18_annotateBACs/HMA4/03_augustus.gtf --hintsfile=18_annotateBACs/HMA4/02_hints.gff --extrinsicCfgFile=src/18_extrinsic.M.RM.E.W.cfg --gff3=on >& log/18.03_augustus_HMA4 
augustus --species=arabidopsis 00_input/MTP1_locus.fa --outfile=18_annotateBACs/MTP1/03_augustus.gtf --hintsfile=18_annotateBACs/MTP1/02_hints.gff --extrinsicCfgFile=src/18_extrinsic.M.RM.E.W.cfg --gff3=on >& log/18.03_augustus_MTP1 
```


### 18.4 Amend annotation

Provide unique IDs for CDS, rename transcript to mRNA (as in TAIR), remove comments, pad gene ids with zeros for correct sorting.

```shell
src/18_fixAugustusGff.pl -i 18_annotateBACs/HMA4/03_augustus.gtf -m -p 2 > 18_annotateBACs/HMA4/04_annotFixed.gff
src/18_fixAugustusGff.pl -i 18_annotateBACs/MTP1/03_augustus.gtf -m -p 2 > 18_annotateBACs/MTP1/04_annotFixed.gff
```


### 18.5 Extract CDS

```shell
src/18_extractCds.pl -f 00_input/HMA4_locus.fa -g 18_annotateBACs/HMA4/04_annotFixed.gff -l -o 18_annotateBACs/HMA4/05_cds.fa
src/18_extractCds.pl -f 00_input/MTP1_locus.fa -g 18_annotateBACs/MTP1/04_annotFixed.gff -l -o 18_annotateBACs/MTP1/05_cds.fa
```




## 19. Call polymorphisms

We will use the frequency of polymorphisms to estimate heterozygosity.

To avoid placing the derived indexes and dictionaries into the `00_input` directory, we symlink the reference FASTA file.

The `19_runCall` script has certain SGE parameters hardcoded. They may need to be adjusted based on the environment where the script is run.

```shell
module load seq/bwa/0.7.12 seq/queue/3.4-0 seq/samtools/1.2 seq/picard/1.130
mkdir -p 19_variants
ln -s ../00_input/Ahal_v2.2.fa 19_variants
nice src/19_runCall -p Ahal_200 -p Ahal_500 -d 00_input -o 19_variants -r 19_variants/Ahal_v2.2.fa -t 8 -s 8 -n Ahal -e '-run' &> log/19_variants &
```

Add TE information to the output. To be conservative, we would only ignore variants located within TE. Hence, we can set maximum distance to 1. If it is adjacent to a transposable element, it is most likely a false positive.

```shell
src/19_addTeToVariants.R -v 19_variants/filtered.txt.xz -t 10_te/RepeatMasker/RmTe.gff -o 19_variants/filtered_wte.txt --maxDist 1
xz 19_variants/filtered_wte.txt
```




## 20. Assess heterozygosity

We only consider variants with genotype quality (GQ) of at least 20. All missing data and bases without coverage in the reference are ignored.

```R
require(stringr)
dfa <- read.delim("19_variants/filtered_wte.txt.xz", stringsAsFactors=F)
dfa$AlleleN <- as.vector(sapply(dfa$Ahal.AD, function(x) sum(as.integer(unlist(str_split(x, ","))) > 0)))
genomeLen <- scan("19_variants/05_coveredBases.txt")
sum(dfa$Ahal.GQ >= 20 & dfa$AlleleN > 1 & is.na(dfa$TE.Dist)) / genomeLen * 100
```



## 21. AHRD annotation

For input, AHRD requires BLAST hits against, for instance, uniprot. We will use the manually annotated Swiss-Prot database and automatically annotated TrEMBL. It also needs fasta file with protein sequences. So, we have to translate all our CDS.

Translate CDS

`transeq` automatically adds `_1` prefix, which we do not need. We also do not need the location. Hence, we remove everything after the underscore.

```shell
mkdir 21_ahrd
module load bio/emboss/6.5.7
transeq <(xz -dc 17_augFinal/cds.fa.xz) -trim -stdout -auto | sed -r '/^>/ s/^(>[^_]+).*/\1/' > 21_ahrd/proteins.fa
```

Blast the proteins against the sprot and trembl. Set `db` to the location of the uniprot database. The trembl run is very slow and might take 4-5 days even when using 16 cores.

```shell
module load bio/blast/2.2.30
BLASTDB=$db/uniprot_20160331 blastp -query 21_ahrd/proteins.fa -db sprot -evalue 1e-5 -outfmt 6 -out 21_ahrd/hits_sprot.txt
BLASTDB=$db/uniprot_20160331 blastp -query 21_ahrd/proteins.fa -db trembl -evalue 1e-75 -outfmt 6 -out 21_ahrd/hits_trembl.txt
chmod 444 21_ahrd/*.txt
```

Unfortunately, the tool requires uncompressed fasta files used to create BLAST databases. Since the files are huge, they were temporarily unarchived from their permanent location into the target directory.

```shell
xz -dc $db/uniprot_20160331/sprot.fasta.xz > 21_ahrd/sprot_20160331.fasta
xz -dc $db/uniprot_20160331/trembl.fasta.xz > 21_ahrd/trembl_20160331.fasta
```

Run the application. Blacklist, filter, and token_blacklist are all required parameters. If you do not specify them, the application will fail with a cryptic NullPointer exception. GO annotation fails with a MySQL query error.

```shell
module load seq/ahrd/3.11
java -Xmx32g -jar $ahrd_jar src/21_ahrd_go.yml
```

Add AHRD attributes to the GFF file.

```shell
appendAhrd.pl --annot 17_augFinal/annotation.gff.xz --ahrd 21_ahrd/output.csv | xz > 21_ahrd/annotation_ahrd.gff.xz
```

Remove the uncompressed files.

```shell
rm 21_ahrd/{sprot,trembl}_20160331.fasta
```



## 22. Pipeline flowcharts

Generate flowcharts of the assembly and annotation pipeline.

```shell
mkdir -p graphs/22_flowcharts
cd src/22_flowcharts
pdflatex -interaction=nonstopmode -shell-escape -output-directory ../../graphs/22_flowcharts assembly.tex
pdflatex -interaction=nonstopmode -shell-escape -output-directory ../../graphs/22_flowcharts annotation.tex
```

